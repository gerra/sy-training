// ЗАДАЧА 1
// Связанная функция как метод
// важность: 5
// Что выведет функция?

// РЕШЕНИЕ ЗАДАЧИ 1
'use strict'

console.log('ЗАДАЧА 1');

/**
 * Выводит в консоль контекст 
 */
function printThis() {
    console.log( this ); // При 'use strict' выдает null, иначе выдает [object window]
}

let user = {
    g: printThis.bind(null) // привязывает функцию к null
};

user.g(); 

// ЗАДАЧА 2 
// Повторный bind
// важность: 5
// Можем ли мы изменить this дополнительным связыванием?

// Что выведет этот код?

//РЕШЕНИЕ ЗАДАЧИ 2
console.log('ЗАДАЧА 2');
/**
 * Выводит имя из контекста
 */
function printName() {
    console.log(this.name);
}

printName = printName
    .bind({ name: "Вася" })
    .bind({ name: "Петя" }); // привязывается только первый аргумент name

printName(); // Вася 

//ЗАДАЧА 3
// Свойство функции после bind
// важность: 5
// В свойство функции записано значение. Изменится ли оно после применения bind? Обоснуйте ответ.

// РЕШЕНИЕ ЗАДАЧИ 3
console.log('ЗАДАЧА 3');
/**
 * Выводит в консоль .name из контекста
 */
function sayHi() {
    console.log(this.name);
}

sayHi.test = 5;

let bound = sayHi.bind({
    name: "Вася"
});

bound = sayHi; // Тогда при запуске bound.test выдаст 5

console.log(bound.test); // undefined, потому что привязывался только name: "Вася"

// ЗАДАЧА 4

// Исправьте функцию, теряющую "this"
// важность: 5
// Вызов askPassword() в приведённом ниже коде должен проверить пароль и затем вызвать user.loginOk/loginFail в зависимости от ответа.
// Однако, его вызов приводит к ошибке. Почему?
// Исправьте выделенную строку, чтобы всё работало (других строк изменять не надо).

// function askPassword(ok, fail) {
//   let password = prompt("Password?", '');
//   if (password == "rockstar") ok();
//   else fail();
// }

// let user = {
//   name: 'Вася',

//   loginOk() {
//     alert(`${this.name} logged in`);
//   },

//   loginFail() {
//     alert(`${this.name} failed to log in`);
//   },

// };

// askPassword(user.loginOk, user.loginFail); 

// РЕШЕНИЕ ЗАДАЧИ 4
console.log('ЗАДАЧА 4');

/**
 * Запрашивает и проверяет пароль
 * @param {*} ok 
 * @param {*} fail 
 */
function askPassword(ok, fail) {
  let password = prompt('Password?', '');
  if (password === 'rockstar') ok();
  else fail();
}

let newUser = {
    name: 'Вася',
    loginOk() {
        console.log(`${this.name} logged in`);
    },
    loginFail() {
        console.log(`${this.name} failed to log in`);
    },
};

askPassword(
    newUser.loginOk.bind(newUser), // мы привязываем loginOk к контексту.
    newUser.loginFail.bind(newUser)
);


// ЗАДАЧА 5

// Использование частично применённой функции для логина
// важность: 5
// Это задание является немного усложнённым вариантом одного из предыдущих – Исправьте функцию, теряющую "this".

// Объект user был изменён. Теперь вместо двух функций loginOk/loginFail у него есть только одна – user.login(true/false).

// Что нужно передать в вызов функции askPassword в коде ниже, чтобы она могла вызывать функцию user.login(true) как ok и функцию user.login(false) как fail?

// function askPassword(ok, fail) {
//   let password = prompt("Password?", '');
//   if (password == "rockstar") ok();
//   else fail();
// }

// let user = {
//   name: 'John',

//   login(result) {
//     alert( this.name + (result ? ' logged in' : ' failed to log in') );
//   }
// };

// askPassword(?, ?); // ?

// РЕШЕНИЕ ЗАДАЧИ 5
console.log('ЗАДАЧА 5');
/**
 * Запрашивает и проверяет пароль
 * @param {*} ok 
 * @param {*} fail 
 */
function askPassword(ok, fail) {
    let password = prompt("Password?", '');
    if (password === "rockstar") ok();
    else fail();
}

let userLogin = {
    name: 'John',
    login(result) {
        console.log(`${this.name} ${result ? 'logged in' : 'failed to log in'}`);
    }
};

// Привязываем login к userLogin и булевому выражению.
askPassword(userLogin.login.bind(userLogin, true), userLogin.login.bind(userLogin, false));
