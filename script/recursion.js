// ЗАДАЧА 1

// Вычислить сумму чисел до данного
// важность: 5
// Напишите функцию sumTo(n), которая вычисляет сумму чисел 1 + 2 + ... + n.

// Сделайте три варианта решения:

// С использованием цикла.
// Через рекурсию, т.к. sumTo(n) = n + sumTo(n-1) for n > 1.
// С использованием формулы арифметической прогрессии.
// Пример работы вашей функции:

// function sumTo(n) { /*... ваш код ... */ }

// alert( sumTo(100) ); // 5050
// P.S. Какой вариант решения самый быстрый? Самый медленный? Почему?

// P.P.S. Можно ли при помощи рекурсии посчитать sumTo(100000)?

// РЕШЕНИЕ ЗАДАЧИ 1.1. ЦИКЛ.
console.log('ЗАДАЧА 1.1. ЦИКЛ');

function sumTo(n) {
    let x = 0;
    for (let i = 0; i < (n + 1); i++) { // нужно добавить 1, чтобы включить в цикл i = n
        x += i;
    }
    return x;
}

console.log(sumTo(5)); // 1+2+3+4+5 = 15
console.log(sumTo(10)); // 1+2+3+4+5+6+7+8+9+10 = 55
console.log(sumTo(100)); // 5050

// РЕШЕНИЕ ЗАДАЧИ 1.2. РЕКУРСИЯ.
console.log('ЗАДАЧА 1.2. РЕКУРСИЯ.');

function sumByRec(a) {
    if (a === 0) {
        return a;
    } else {
        return a + sumByRec(a - 1);
    }
}

console.log(sumByRec(5)); // 5+4+3+2+1+0 = 15
console.log(sumByRec(10)); // 10+9+8+7+6+5+4+3+2+1 = 55;
console.log(sumByRec(100)); // 5050;

// РЕШЕНИЕ ЗАДАЧИ 1.3. ПРОГРЕССИЯ
console.log('ЗАДАЧА 1.2. ПРОГРЕССИЯ');

function sumPr(a) {
    let sumA = (1 + a)/2 * a;
    return sumA;
}

console.log(sumPr(5)); //15
console.log(sumPr(10)); //55
console.log(sumPr(100));//5050

// ЗАДАЧА 2

// Вычислить факториал
// важность: 4
// Факториал натурального числа – это число, умноженное на "себя минус один", затем на "себя минус два", и так далее до 1. Факториал n обозначается как n!
// Определение факториала можно записать как:
// n! = n * (n - 1) * (n - 2) * ...*1
// Примеры значений для разных n:

// 1! = 1
// 2! = 2 * 1 = 2
// 3! = 3 * 2 * 1 = 6
// 4! = 4 * 3 * 2 * 1 = 24
// 5! = 5 * 4 * 3 * 2 * 1 = 120
// Задача – написать функцию factorial(n), которая возвращает n!, используя рекурсию.

// alert( factorial(5) ); // 120
// P.S. Подсказка: n! можно записать как n * (n-1)! Например: 3! = 3*2! = 3*2*1! = 6

// РЕШЕНИЕ ЗАДАЧИ 2

console.log('ЗАДАЧА 2');

function factorial(c) {
    if (c === 1) {
        return c;
    } else {
        return c * factorial(c - 1);
    }
}

console.log(factorial(5)); // 5*4*3*2*1 = 120;


// ЗАДАЧА 3

// Числа Фибоначчи
// важность: 5
// Последовательность чисел Фибоначчи определяется формулой Fn = Fn-1 + Fn-2. То есть, следующее число получается как сумма двух предыдущих.
// Первые два числа равны 1, затем 2(1+1), затем 3(1+2), 5(2+3) и так далее: 1, 1, 2, 3, 5, 8, 13, 21....
// Числа Фибоначчи тесно связаны с золотым сечением и множеством природных явлений вокруг нас.
// Напишите функцию fib(n) которая возвращает n-е число Фибоначчи.
// Пример работы:
// function fib(n) { /* ваш код */ }
// alert(fib(3)); // 2
// alert(fib(7)); // 13
// alert(fib(77)); // 5527939700884757
// P.S. Все запуски функций из примера выше должны работать быстро. Вызов fib(77) должен занимать не более доли секунды.

// РЕШЕНИЕ ЗАДАЧИ 3

console.log('ЗАДАЧА 3');

function fib(f) {
    let arr = [0, 1];
    arr.length = f;
    for (let i = 2; i < f; i++) {
        arr[i] = arr[i - 1] + arr[i - 2];
    }
    return `Число Фиббоначи для ${f} = ${arr[f-1]}`;
}
console.log(fib(10)); //55
console.log(fib(77)); //5527939700884757

function rfib(j) {
    if (j <= 1) {
        return j;
    } else {
        return rfib(j - 2) + rfib(j - 1);
    }
}
console.log(rfib(10));

// ЗАДАЧА 4

// Вывод односвязного списка
// важность: 5
// Допустим, у нас есть односвязный список (как описано в главе Рекурсия и стек):
// let list = {
//   value: 1,
//   next: {
//     value: 2,
//     next: {
//       value: 3,
//       next: {
//         value: 4,
//         next: null
//       }
//     }
//   }
// };
// Напишите функцию printList(list), которая выводит элементы списка по одному.
// Сделайте два варианта решения: используя цикл и через рекурсию.
// Как лучше: с рекурсией или без?

// РЕШЕНИЕ ЗАДАЧИ 4.1. ЦИКЛ
console.log('ЗАДАЧА 4.1 ЦИКЛ');

let list = {
  value: 1,
  next: {
    value: 2,
    next: {
      value: 3,
      next: {
        value: 4,
        next: null
      }
    }
  }
};

function printList(list) {
    while (list) {
        console.log(list.value);
        list = list.next;
    }
}
printList(list);

// РЕШЕНИЕ ЗАДАЧИ 4.2. РЕКУРСИЯ
console.log('ЗАДАЧА 4.2. РЕКУРСИЯ');

function rPrintList(list) {
    console.log(list.value);
    if (list.next) {
        rPrintList(list.next);
    }
}

rPrintList(list);

// ЗАДАЧА 5.
// Вывод односвязного списка в обратном порядке
// важность: 5
// Выведите односвязный список из предыдущего задания Вывод односвязного списка в обратном порядке.
// Сделайте два решения: с использованием цикла и через рекурсию.

// РЕШЕНИЕ ЗАДАЧИ 5.1. ЦИКЛ
console.log('ЗАДАЧА 5.1. ЦИКЛ')

let newList = {
    value: 1,
    next: {
      value: 2,
      next: {
        value: 3,
        next: {
          value: 4,
          next: null
        }
      }
    }
  };

function reverseList(newList) {
    console.log({newList});
    let newArr = [];
    console.log({newArr});
    while (newList) {
        newArr.push(newList.value);
        newList = newList.next;
    }
    for (let i = newArr.length - 1; i >= 0; i--) {
        console.log(newArr[i]);
    }
}

reverseList(newList);

// РЕШЕНИЕ ЗАДАЧИ 5.2. РЕКУРСИЯ
console.log('ЗАДАЧА 5.2. РЕКУРСИЯ')

function rReverseList(newList) {
    if (newList.next) {
        rReverseList(newList.next);
    }
    console.log(newList.value);
}

rReverseList(newList);

