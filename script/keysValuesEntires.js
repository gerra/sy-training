// ЗАДАЧА 1

// Сумма свойств объекта
// важность: 5
// Есть объект salaries с произвольным количеством свойств, содержащих заработные платы.
// Напишите функцию sumSalaries(salaries), которая возвращает сумму всех зарплат с помощью метода Object.values и цикла for..of.
// Если объект salaries пуст, то результат должен быть 0.
// Например:
// let salaries = {
//   "John": 100,
//   "Pete": 300,
//   "Mary": 250
// };
// alert( sumSalaries(salaries) ); // 650

//Решение ЗАДАЧИ 1

console.log('ЗАДАЧА 1');

let salaries = {
  "John": 100,
  "Pete": 300,
  "Mary": 200,
  "Ben": 400,
};

function sumSalaries(salaries) {
    let set = new Set(Object.values(salaries)); // создаем set из значений массива salaries
    let sum = 0; // вводим переменную для суммирования
    set.forEach((value) => sum += value); // запускаем цикл, который пробегает по всем value объекта set и плюсует их в sum
    return sum;
}
console.log (sumSalaries(salaries)); // выводит в консоль 1000

// ЗАДАЧА 2
// Подсчёт количества свойств объекта
// важность: 5
// Напишите функцию count(obj), которая возвращает количество свойств объекта:
// let user = {
//   name: 'John',
//   age: 30
// };
// alert( count(user) ); // 2
// Постарайтесь сделать код как можно короче.
// P.S. Игнорируйте символьные свойства, подсчитывайте только «обычные».

// РЕШЕНИЕ ЗАДАЧИ 2
console.log('ЗАДАЧА 2');

let user = {
  name: 'John',
  age: 30,
  phone: '+9-999-000-0000',
  birth: '31.12.1990'
};

function count(obj) {
    let newSet = new Set(Object.keys(obj)); // Создаем set из свойств объекта obj
    let newSum = 0; // создаем переменную для складывания
    newSet.forEach(() => newSum += 1);// запускаем цикл в котором считаем количество свойств
    return newSum; // возвращаем количество свойств
};
console.log(count(user)); // выводит в консоль: 4
