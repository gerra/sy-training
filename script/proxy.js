// ЗАДАЧА 1

// Ошибка при чтении несуществующего свойства
// Обычно при чтении несуществующего свойства из объекта возвращается undefined.

// Создайте прокси, который генерирует ошибку при попытке прочитать несуществующее свойство.

// Это может помочь обнаружить программные ошибки пораньше.

// Напишите функцию wrap(target), которая берёт объект target и возвращает прокси, добавляющий в него этот аспект функциональности.

// Вот как это должно работать:

// let user = {
//   name: 'John'
// };

// function wrap(target) {
//   return new Proxy(target, {
//       /* ваш код */
//   });
// }

// user = wrap(user);

// console.log(user.name); // John
// console.log(user.age); // Ошибка: такого свойства не существует


// РЕШЕНИЕ ЗАДАЧИ 1

console.log('ЗАДАЧА 1');

let user = {
    name: 'John'
};

function wrap(target) {
    return new Proxy(
        target, 
        {
            get(target, key) {
                if (key in target) {
                    return target[key];
                } else {
                    throw new Error(`Ошибка: такого свойства не существует: ${key}`);
                }
            }
        }
    );
}

user = wrap(user);

console.log(user.name); // John
console.log(user.age); // Ошибка: такого свойства не существует


// ЗАДАЧА 2

// Получение элемента массива с отрицательной позиции
// В некоторых языках программирования возможно получать элементы массива, используя отрицательные индексы, отсчитываемые с конца.

// Вот так:

// let array = [1, 2, 3];

// array[-1]; // 3, последний элемент
// array[-2]; // 2, предпоследний элемент
// array[-3]; // 1, за два элемента до последнего
// Другими словами, array[-N] – это то же, что и array[array.length - N].

// Создайте прокси, который реализовывал бы такое поведение.

// Вот как это должно работать:

// let array = [1, 2, 3];

// array = new Proxy(array, {
//   /* ваш код */
// });

// console.log( array[-1] ); // 3
// console.log( array[-2] ); // 2

// РЕШЕНИЕ ЗАДАЧИ 2

console.log('ЗАДАЧА 2');

let array = [1, 2, 3];

array = new Proxy(
    array, 
    {
        get(target, prop) {
            let newProp = 0;
            if (prop < 0) {
                newProp = +prop + target.length;
            }
            return target[newProp];
        }
    }
);

console.log(array[-1]); // 3
console.log(array[-2]); // 2


// ЗАДАЧА 3

// Observable
// Создайте функцию makeObservable(target), которая делает объект «наблюдаемым», возвращая прокси.

// Вот как это должно работать:

// function makeObservable(target) {
//   /* ваш код */
// }

// let user = {};
// user = makeObservable(user);

// user.observe((key, value) => {
//   alert(`SET ${key}=${value}`);
// });

// user.name = "John"; // выводит: SET name=John
// Другими словами, возвращаемый makeObservable объект аналогичен исходному, но также имеет метод observe(handler), который позволяет запускать handler при любом изменении свойств.

// При изменении любого свойства вызывается handler(key, value) с именем и значением свойства.

// P.S. В этой задаче ограничьтесь, пожалуйста, только записью свойства. Остальные операции могут быть реализованы похожим образом.


// РЕШЕНИЕ ЗАДАЧИ 3

console.log('ЗАДАЧА 3');

let handlers = Symbol('handlers');

function makeObservable(target) {
    target[handlers] = [];

    target.observe = function (handler) {
        this[handlers].push(handler);
    };

    return new Proxy(
        target, 
        {
            set(target, property, value, reciever) {
                let success = Reflect.set(...arguments);
                if (success) {
                    target[handlers].forEach(handler => handler(property, value));
                }
                return success;
            }
        }
    )
}

let user = {};
user = makeObservable(user);

user.observe((key, value) => {
    console.log(`SET ${key}=${value}`);
});

user.name = "John";
