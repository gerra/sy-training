// ЗАДАЧА 1


// Изменяем "prototype"
// важность: 5
// В коде ниже мы создаём нового кролика new Rabbit, а потом пытаемся изменить его прототип.

// Сначала у нас есть такой код:

// function Rabbit() {}
// Rabbit.prototype = {
//     eats: true
// };

// let rabbit = new Rabbit();

// console.log(rabbit.eats); // true
// Добавим одну строчку (выделенную в коде ниже). Что вызов alert покажет нам сейчас?

function Rabbit() {}
Rabbit.prototype = {
    eats: true
};

let rabbit = new Rabbit();

Rabbit.prototype = {};

console.log(rabbit.eats); // true

// …А если код такой (заменили одну строчку)?

function RabbitOne() {}
RabbitOne.prototype = {
    eats: true
};

let rabbitOne = new RabbitOne();

RabbitOne.prototype.eats = false;

console.log(rabbitOne.eats); // false // потому что .eats был перезаписан

// Или такой (заменили одну строчку)?

function RabbitTwo() {}
RabbitTwo.prototype = {
    eats: true
};

let rabbitTwo = new RabbitTwo();

delete rabbitTwo.eats;

console.log(rabbitTwo.eats); // true

// Или, наконец, такой:

function RabbitThree() {}
RabbitThree.prototype = {
    eats: true
};

let rabbitThree = new RabbitThree();

delete RabbitThree.prototype.eats;

console.log(rabbitThree.eats); // undefined


// ЗАДАЧА 2

// Создайте новый объект с помощью уже существующего
// важность: 5
// Представьте, что у нас имеется некий объект obj, созданный функцией-конструктором – мы не знаем какой именно, но хотелось бы создать ещё один объект такого же типа.

// Можем ли мы сделать так?

// let obj2 = new obj.constructor();
// Приведите пример функции-конструктора для объекта obj, с которой такой вызов корректно сработает. И пример функции-конструктора, с которой такой код поведёт себя неправильно.

// РЕШЕНИЕ ЗАДАЧИ 2

console.log('ЗАДАЧА 2');

function Student(name, age, specialty, phone) {
    this.name = name;
    this.age = age;
    this.specialty = specialty;
    this.phone = phone;
}

let student = new Student(
    'Adam', 
    20, 
    'web dev', 
    '8-999-888-77-66'
);

let newStudent = new student.constructor(
    'Eve', 
    19, 
    'back-end', 
    '8-999-888-77-67'
);

for (let key in student) {
    console.log(key); // name, age, specialty, phone
}

for (let key in student) {
    console.log(student[key]); // Adam, 20, web dev, 8-999-888-77-66
}

for (let key in newStudent) {
    console.log(newStudent[key]); // Eve, 19, back-end, 8-999-888-77-6gf
}
