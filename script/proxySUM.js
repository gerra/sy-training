// КОНСПЕКТ
// "use strict";

// ловушка GET 

let num = [0, 1, 2, 3, 4, 5];
/**
 * мы здесь оборачиваем массив num в прокси
 * Если в массиве есть вызываемый ключ, то возвращается его значение
 * Если в массиве нет вызываемого ключа, то возвращается 0
 */
num = new Proxy(num, {
    get(target, key) {
        if (key in target) {
            return target[key]
        } else {
            return 0;
        }
    }
});

console.log(num[0]); // 0
console.log(num[1]); // 1
console.log(num[2]); // 2
console.log(num[6]); // 0
console.log(num[7]); // 0
console.log(num[8]); // 0


// СЛОВАРЬ

let dictionary = {
    'hello': 'Привет',
    'bye': 'Пока',
    'table': 'Стол',
};

dictionary = new Proxy(dictionary, {
    get(target, key) {
        if (key in target) {
            return target[key];
        } else {
            return key;
        }
    }
})

console.log(dictionary['Hello']); // Привет
console.log(dictionary['Bye']); // Пока
console.log(dictionary['Table']); // Стол
console.log(dictionary['Church']); // Church

dictionary.church = 'Церковь';
console.log(dictionary['Church']); // Church

dictionary.bible = 'Библия';
console.log


// ПЕРЕБОР ПРИ ПОМОЩИ OUNKEYS

let user = {
    name: 'Joseph',
    age: 17,
    country: 'Egypt',
    _password: 'Israel',
    _parent: 'Israel'
};

user = new Proxy(user, {
    ownKeys(target) {
        return Object.keys(target).filter(key => !key.startsWith('_'));
    }
});

for (let key in user) {
    console.log(key); // name, age, country
}

console.log(Object.keys(user)); // [ 'name', 'age', 'country' ]
console.log(Object.values(user)); // [ 'Joseph', 17, 'Egypt' ]


// ЗАЩИЩЕННЫЕ СВОЙСТВА

let newUser = {
    name: 'David',
    age: 20,
    weapon: 'sling',
    wins: 'Goliath',
    country: 'Israel',
    _password: 'Psalm_1'
}

console.log(newUser._password); // Psalm_1
console.log(Object.keys(newUser)); // [ 'name', 'age', 'weapon', 'wins', 'country', '_password' ]
console.log(Object.values(newUser)); // [ 'David', 20, 'sling', 'Goliath', 'Israel', 'Psalm_1' ]
console.log({newUser}); // newUser: {...}

newUser = new Proxy(newUser, {
    get(target, prop) { // Перехват чтения защищенных свойств
        if (prop.startsWith('_')) {
            throw new Error('Отказано в доступе!');
        } else {
            let value = target[prop];
            return (typeof value === 'function') ? value.bind(target) : value;
        }
    },
    set(target, prop, val) { // Перехват записи защищенных свойств
        if (prop.startsWith('_')) {
            throw new Error('Отказано в доступе!');
        } else {
            target[prop] = val;
            return true; // ??? Почему здесь нужно возвращать true ???
        }
    },
    deleteProperty(target, prop) { // Перехват удаления защищенных свойств
        if (prop.startsWith('_')) {
            throw new Error('Отказано в доступе!');
        } else {
            delete target[prop];
            return true; // ??? Почему здесь тоже нужно возвращать true ???
        }
    },
    ownKeys(target) { // Перехват итерации защищенных свойств
        return Object.keys(target).filter(key => !key.startsWith('_'));
    }
})

try {
    console.log(newUser._password); // Error: Отказано в доступе
  } catch(e) { console.log(e.message); }
  
  // "set" не позволяет записать _password
  try {
    newUser._password = "test"; // Error: Отказано в доступе
  } catch(e) { console.log(e.message); }
  
  // "deleteProperty" не позволяет удалить _password
  try {
    delete newUser._password; // Error: Отказано в доступе
  } catch(e) { console.log(e.message); }
  
  // "ownKeys" исключает _password из списка видимых для итерации свойств
  for(let key in newUser) console.log(key); // name

  console.log({newUser});