//МЕТОДЫ МАССИВОВ

//МЕТОД arr.splice(1, 1)
//Удаление
let arr = ['Я', 'изучаю', 'Javascript'];
console.log(arr);
arr.splice(1,1);
console.log(arr);

let arr1 = ['Я', 'изучаю', 'Javascript', 'прямо', 'сейчас'];
console.log(arr1);
arr1.splice(0,3, 'Давай', 'бегать');
console.log(arr1); // Давай бегать прямо сейчас

//splice возвращает массив из удаленных элементов
let arr2 = ['Я', 'изучаю', 'Javascript', 'прямо', 'сейчас'];
let removed = arr2.splice(0, 2);
console.log(removed); // Я изучаю
console.log(arr2); //JavaScript прямо сейчас

//splice может вставлять элементы без удаления:
let arr3 = ['Я', 'изучаю', 'JavaScript'];
console.log(arr3);
arr3.splice(2, 0, 'очень', 'интересный', 'язык программирования');
console.log(arr3);

//splice может использовать отрицательные числа
let arr4 = [1, 2, 5];
// начиная с индекса -1 (перед последним элементом)
// удалить 0 элементов,
// затем вставить числа 3 и 4
arr4.splice(-1, 0, 3, 4);

//МЕТОД SLICE
let arr5 = [1, 2, 3, 4, 5, 6];
console.log(arr5.slice(1,3));//2, 3
console.log(arr5);//[ 1, 2, 3, 4, 5, 6 ]
console.log(arr5.slice(-3));//[ 4, 5, 6 ]
console.log(arr5);

//МЕТОД CONCAT
let arr6 = [1, 2];
console.log(arr6.concat([3,4]));
console.log(arr6.concat([3, 4], [5, 6]));
console.log(arr6.concat([3, 4], 5, 6));

arrNew = arr6.concat([3, 4]);
console.log(arrNew);

//Пример использования метода slice()
let myHonda = {
    color: 'красный',
    wheels: 4,
    engine: {
        cylinders: 4,
        size: 2.2,
    },
};
console.log(myHonda);
let myCar = [
    myHonda,
    2,
    'в хорошем сотоянии',
    'приобретена в 1997',];
//Создаем newCar из myCar:
let newCar = myCar.slice(0,2);
//Отображаем значения myCar, newCar и цвет myHonda по ссылкам из обоих массивов
console.log('myCar =', myCar);
console.log(`newCar =`, newCar);
console.log('myCar[0].color =', myCar[0].color);
console.log('newCar[0].color =', newCar[0].color);
//Изменяем цвет myHonda:
myHonda.color = 'черный';
console.log('Новый цвет моей Honda:', myHonda.color);
//Отображаем цвет myHonda в обоих массивах:
console.log('myCar[0].color =', myCar[0].color);
console.log('newCar[0].color =', newCar[0].color);

//ПЕРЕБОР FOREACH
let arr7 = [1, 2, 3, 4, 5, 6, 7];
arr7.forEach((item, index) => {console.log(item, index + 1)});
console.log(arr7);

let newArr7 = arr7.map(function (item, index, array) {
    return item + 1;
    });

console.log(newArr7);

//ПОИСК В МАССИВЕ
let arr8 = [1, 2, 3, 4, 5, 6, 7];
console.log(arr8.indexOf(0));//-1
console.log(arr8.indexOf(3));//2
console.log(arr8.indexOf(7));//6 возвращает индекс, на котором найден элемент

//MAP вызывает функцию для каждого элемента
//массива и возвращает массив результатов
//выполнения этой функции
let arr9 = ['Mattew', 'Mark', 'Luke', 'John'];
let result = arr9.map(item => item.length);
console.log(result);

//SORT(FN)
let arr10 = [10, 9, 8, 12, 7, 6, 5, 4, 3, 2, 23];
arr10.sort();
console.log(arr10); //10, 12, 2, 23, 3,4, 5, 6, 7, 8, 9
//функция для устранения некорректной сортировки
function compareNum(a, b) {
    if (a > b) return 1;
    if (a == b) return 0;
    if (a < b) return -1;
}
arr10.sort(compareNum);
console.log(arr10);

//Функция сравнения может вернуть любое число
//Функции сравнения нужно любое '+' число, чтобы
// сказать "Больше" и любое '-' число, 
//чтобы сказать "меньше".
let arr11 = [10, 9, 8, 12, 7, 6, 5, 4, 3, 2, 23];
console.log(arr11);
arr11.sort(function(a, b) {return a - b});
console.log(arr11);

//Стрелочные функции выглядят аккуратнее.
let arr12 = [10, 9, 8, 12, 7, 6, 5, 4, 3, 2, 23];
arr12.sort((a, b) => a - b);
console.log(arr12);

//REVERSE
let arr13 = [1, 2, 3, 4, 5, 6];
arr13.reverse();
console.log(arr13);// 6, 5, 4, 3, 2, 1

//SPLIT разбивает строку на массив
let names = 'Moses, Aaron, Hur, Ieshua, Caleb';
let arr14 = names.split(', ');
for (let name of arr14) {
    console.log(`Message sending to: ${name}.`);
}

//.JOIN(GLUE) создает строку из массива
let arr15 = ['Moses', 'Aaron', 'Hur', 'Ieshua', 'Caleb'];
let str15 = arr15.join(', ');
console.log(`Heroes of Old Teastment: ${str15}`);

//REDUCE
let arr16 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let result16 = arr16.reduce((sum, current) => sum + current, 0);
console.log(result16);//55
//Можно опустить 0 (initial)
let result17 = arr16.reduce((sum, current) => sum + current);
console.log(result16);//55 

//Array.isArray - проверка на массив
console.log(Array.isArray(arr16));//true

//ЗАДАЧА1
/*
Переведите текст вида border-left-width в borderLeftWidth
важность: 5
Напишите функцию camelize(str), которая преобразует строки вида «my-short-string» в «myShortString».
То есть дефисы удаляются, а все слова после них получают заглавную букву.
Примеры:
camelize("background-color") == 'backgroundColor';
P.S. Подсказка: используйте split, чтобы разбить строку на массив символов, потом переделайте всё как нужно и методом join соедините обратно.
*/
//РЕШЕНИЕ ЗАДАЧИ 1
function camelize(string00) {
    let arr00 = string00.split('-');
    console.log(arr00);
    let arr01 = arr00.map((item, number) => 
        number === 0 
            ? item 
            : item[0].toUpperCase() + item.slice(1));
    console.log(arr01);
    let string01 = arr01.join('');
    return string01;
}
console.log(camelize('background-color'));
console.log(camelize('working-space'));
console.log(camelize('one-two-three-four'));

//ЗАДАЧА 2
/*
Фильтрация по диапазону
важность: 4
Напишите функцию filterRange(arr, a, b), которая принимает массив arr, ищет в нём элементы между a и b и отдаёт массив этих элементов.
Функция должна возвращать новый массив и не изменять исходный.
ПРИМЕР:
let arr = [5, 3, 8, 1];
let filtered = filterRange(arr, 1, 4);
alert( filtered ); // 3,1 (совпадающие значения)
alert( arr ); // 5,3,8,1 (без изменений)
*/

//РЕШЕНИЕ ЗАДАЧИ 2
function filterRange(arr02, a, b) {
    let min02 = a < b ? a : b; // Проверка, если a > b
    let max02 = a > b ? a : b;
    let newArr02 = arr02.filter(item => (item >= min02 && item <= max02));
    return newArr02;
}
let arr02 = [5, 3, 8, 1];
let filtered = filterRange(arr02, 1, 4);
console.log(filtered); //3, 1
arr02 = [5, 3, 8, 1, 12, 22, 21, 11, 9];
filtered = filterRange(arr02, 1, 11);
console.log(filtered); //5, 3, 8, 1, 11, 9

//ЗАДАЧА 3
/*
Фильтрация по диапазону "на месте"
важность: 4
Напишите функцию filterRangeInPlace(arr, a, b), которая принимает массив arr и удаляет из него все значения кроме тех, которые находятся между a и b. То есть, проверка имеет вид a ≤ arr[i] ≤ b.
Функция должна изменять принимаемый массив и ничего не возвращать.
Например:
let arr = [5, 3, 8, 1];
filterRangeInPlace(arr, 1, 4); // удалены числа вне диапазона 1..4
alert( arr ); // [3, 1]
*/

//РЕШЕНИЕ ЗАДАЧИ 3
function filterRangeInPlace(arr03, a, b) {
    let min03 = a < b ? a : b;
    let max03 = a > b ? a : b;
    arr03.forEach((value, index) => {
        if (value < min03 || value > max03)
            arr03.splice(index, 1)
        })
    return arr03;
}

let arr03 = [5, 3, 8, 2, 4, 1];
filterRangeInPlace(arr03, 2, 4)
console.log(arr03);

//ЗАДАЧА 4
/*
Сортировать в порядке по убыванию
важность: 4
let arr = [5, 2, 1, -10, 8];
// ... ваш код для сортировки по убыванию
alert( arr ); // 8, 5, 2, 1, -10
*/

//РЕШЕНИЕ ЗАДАЧИ 4
let arr04 = [5, 2, 1, -10, 8];
arr04.sort( (a, b) => b - a);
console.log(arr04);

//ЗАДАЧА 5
/*
Скопировать и отсортировать массив
важность: 5
У нас есть массив строк arr. Нужно получить отсортированную копию, но оставить arr неизменённым.
Создайте функцию copySorted(arr), которая будет возвращать такую копию.
let arr = ["HTML", "JavaScript", "CSS"];
let sorted = copySorted(arr);
alert( sorted ); // CSS, HTML, JavaScript
alert( arr ); // HTML, JavaScript, CSS (без изменений)
*/

//РЕШЕНИЕ ЗАДАЧИ 5
function copySorted (arr05) {
    let copy = arr05.slice().sort();
    return copy;
}
let arr05 = ["HTML", "JavaScript", "CSS"];
let sorted = copySorted(arr05);
console.log(sorted);
console.log(arr05);

//ЗАДАЧА 6
/*
Создать расширяемый калькулятор
важность: 5
Создайте функцию конструктор Calculator, которая создаёт «расширяемые» объекты калькулятора.
Задание состоит из двух частей.
Во-первых, реализуйте метод calculate(str), который принимает строку типа "1 + 2" в формате «ЧИСЛО оператор ЧИСЛО» (разделено пробелами) и возвращает результат. Метод должен понимать плюс + и минус -.
Пример использования:
let calc = new Calculator;
alert( calc.calculate("3 + 7") ); // 10
Затем добавьте метод addMethod(name, func), который добавляет в калькулятор новые операции. Он принимает оператор name и функцию с двумя аргументами func(a,b), которая описывает его.
Например, давайте добавим умножение *, деление / и возведение в степень **:
let powerCalc = new Calculator;
powerCalc.addMethod("*", (a, b) => a * b);
powerCalc.addMethod("/", (a, b) => a / b);
powerCalc.addMethod("**", (a, b) => a ** b);
let result = powerCalc.calculate("2 ** 3");
alert( result ); // 8
Для этой задачи не нужны скобки или сложные выражения.
Числа и оператор разделены ровно одним пробелом.
Не лишним будет добавить обработку ошибок.
 */

//РЕШЕНИЕ ЗАДАЧИ 6
function Calculator () {
    this['+'] = (x06, y06) => x06 + y06;
    this['-'] = (x06, y06) => x06 - y06;
    this.calculate = (str06) => {
        let arr06 = str06.split(' ');
        return this[arr06[1]](arr06[0], arr06[2]);
    }
    this.addMethod = (name06, func06) => this[name06] = func06;
}

let calc = new Calculator;
console.log(calc.calculate('3 + 3'));

let powerCalc = new Calculator;
powerCalc.addMethod('*');
powerCalc.addMethod('/');
powerCalc.addMethod('**');
let result = powerCalc.calculate("2 ** 3");
console.log(result);

//ЗАДАЧА 7
/*
Трансформировать в массив имён
важность: 5
У вас есть массив объектов user, и в каждом из них есть user.name. Напишите код, который преобразует их в массив имён.
*/

//РЕШЕНИЕ ЗАДАЧИ 7
let vasya = { name: "Вася", age: 25 };
let petya = { name: "Петя", age: 30 };
let masha = { name: "Маша", age: 28 };
let users = [ vasya, petya, masha ];

let names7 = users.map(item => item.name);

console.log(names7);

//ЗАДАЧА 8
/*
Трансформировать в объекты
важность: 5
У вас есть массив объектов user, и у каждого из объектов есть name, surname и id.
Напишите код, который создаст ещё один массив объектов с параметрами id и fullName, где fullName – состоит из name и surname.
*/

//РЕШЕНИЕ ЗАДАЧИ 8
vasya = {name: "Вася", surname: "Пупкин", id: 1};
petya = {name: "Петя", surname: "Иванов", id: 2};
masha = {name: "Маша", surname: "Петрова", id: 3};

let users8 = [vasya, petya, masha];

let usersMapped = users8.map(users8 => ({
    id: users8.id,
    fullName: `${users8.name} ${users8.surname}`,
}));

console.log(usersMapped);

//ЗАДАЧА 9
/*
Отсортировать пользователей по возрасту
важность: 5
Напишите функцию sortByAge(users), которая принимает массив объектов со свойством age и сортирует их по нему.
*/
function sortByAge(arr09) {
    arr09.sort(function(a, b) {
        return a.age - b.age;
    })
    return arr09;
}

vasya = { name: "Вася", age: 25 };
petya = { name: "Петя", age: 30 };
masha = { name: "Маша", age: 28 };

let arr09 = [ vasya, petya, masha ];

console.log(sortByAge(arr09));
// { name: 'Вася', age: 25 },
// { name: 'Маша', age: 28 },
// { name: 'Петя', age: 30 }

//ЗАДАЧА 10
/*
Перемешайте массив
важность: 3
Напишите функцию shuffle(array), которая перемешивает (переупорядочивает случайным образом) элементы массива.
Многократные прогоны через shuffle могут привести к разным последовательностям элементов. 
*/

//РЕШЕНИЕ ЗАДАЧИ 10
function shuffle(arr10) {
    arr10.sort(function() {
        return 0.5 - Math.random();
    })
    return arr10;
}

arr10 = [1, 2, 3, 4, 5];
console.log(shuffle(arr10));
console.log(shuffle(arr10));
console.log(shuffle(arr10));
console.log(shuffle(arr10));

//ЗАДАЧА 11
/*
Получить средний возраст
важность: 4
Напишите функцию getAverageAge(users), которая принимает массив объектов со свойством age и возвращает средний возраст.
Формула вычисления среднего арифметического значения: (age1 + age2 + ... + ageN) / N.
*/

//РЕШЕНИЕ ЗАДАЧИ 11
function getAverageAge(users11) {
    return users11.reduce((prevValue, user) => prevValue + user.age, 0) / users11.length;
} 

vasya = { name: "Вася", age: 25 };
petya = { name: "Петя", age: 30 };
masha = { name: "Маша", age: 29 };

arr11 = [ vasya, petya, masha ];

console.log(getAverageAge(arr11));

//ЗАДАЧА 12
/*
Оставить уникальные элементы массива
важность: 4
Пусть arr – массив строк.
Напишите функцию unique(arr), которая возвращает массив, содержащий только уникальные элементы arr.
 */

function unique(arr12) {
    console.log({arr12});
    let newArr12 = [];
    arr12.forEach(function(item12) {
        console.log({item12});
        console.log({newArr12});
        if (!newArr12.includes(item12)) {
            newArr12.push(item12);
        }
        console.log({newArr12});
    })
    console.log({newArr12});
    return newArr12;
}

let string = ["кришна", "кришна", "харе", "харе", "харе", "харе", "кришна", "кришна", ":-O"];
console.log(unique(string));
