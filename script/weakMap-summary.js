// КОНСПЕКТ УРОКА

// Реализация счетчика посещений:
let visitersCountMap = new Map(); // Создаем map с парой посетитель - кол-во посещений
function countUser(user) { // создаем функцию для подсчета посещений
    //создаем переменную в которую включаем либо имеющиеся данные по user
    //либо, если такого посетителя в map нет, то просто 0.
    let count = visitersCountMap.get(user) || 0;
    visitersCountMap.set(user, count + 1);
}
// проверяем как это работает:
countUser('Adam');
console.log({visitersCountMap});
countUser('Adam');
countUser('Adam');
countUser('Adam');
countUser('Adam');
console.log({visitersCountMap});
countUser('Eve');
countUser('Eve');
countUser('Eve');
countUser('Eve');
console.log({visitersCountMap}) 
// теперь при вызове функции с именем посетителя получается счетчик:
// 0: {"Adam" => 5}
// 1: {"Eve" => 4}
