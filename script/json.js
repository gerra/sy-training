// ЗАДАЧА 1

// Преобразуйте объект в JSON, а затем обратно в обычный объект
// важность: 5
// Преобразуйте user в JSON, затем прочитайте этот JSON в другую переменную.

// let user = {
//   name: "Василий Иванович",
//   age: 35
// };

// РЕШЕНИЕ ЗАДАЧИ 1

console.log('ЗАДАЧА 1');

let user = {
  name: "Василий Иванович",
  age: 35
};
console.log({user}); // user: {name: "Василий Иванович", age: 35}

let userJson = JSON.stringify(user);
console.log({userJson}); // {userJson: "{\"name\":\"Василий Иванович\",\"age\":35}"}

let newUser = JSON.parse(userJson);
console.log({newUser}); //user: {name: "Василий Иванович", age: 35}


// ЗАДАЧА 2


// Исключить обратные ссылки
// важность: 5
// В простых случаях циклических ссылок мы можем исключить свойство, из-за которого они возникают, из сериализации по его имени.
// Но иногда мы не можем использовать имя, так как могут быть и другие, нужные, свойства с этим именем во вложенных объектах. Поэтому можно проверять свойство по значению.
// Напишите функцию replacer для JSON-преобразования, которая удалит свойства, ссылающиеся на meetup:

<<<<<<< HEAD
// let room = {
//   number: 23
// };

// let meetup = {
//   title: "Совещание",
//   occupiedBy: [{name: "Иванов"}, {name: "Петров"}],
//   place: room
// };

// // цикличные ссылки
// room.occupiedBy = meetup;
// meetup.self = meetup;

// alert( JSON.stringify(meetup, function replacer(key, value) {
//   /* ваш код */
// }));
=======

>>>>>>> a587fc7698cebba051e8a7a2a3da724218adce52


// РЕШЕНИЕ ЗАДАЧИ 2

console.log('ЗАДАЧА 2');

let room = {
  number: 23
};

let meetup = {
  title: "Совещание",
  occupiedBy: [{name: "Иванов"}, {name: "Петров"}],
  place: room
};

// цикличные ссылки
room.occupiedBy = meetup;
meetup.self = meetup;

let meetupJSON = JSON.stringify(meetup, function replacer(key, value) {
<<<<<<< HEAD
    return (key != '' && value == meetup) ? undefined : value; // проверяем value на наличие в ней ссылки meetup
=======
    return (!!key && value === meetup) ? undefined : value; // проверяем value на наличие в ней ссылки meetup
>>>>>>> a587fc7698cebba051e8a7a2a3da724218adce52
});
console.log(meetupJSON); //{"title":"Совещание","occupiedBy":[{"name":"Иванов"},{"name":"Петров"}],"place":{"number":23}}
