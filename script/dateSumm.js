// КОНСПЕКТ

// Создание даты:
let now = new Date();
console.log(now);

let jan01_1970 = new Date(0);
console.log(jan01_1970);

let jan02_1970 = new Date(24 * 3600 * 1000);
console.log(jan02_1970);

let date = new Date("2021-08-10");
console.log(date);

let newDate = new Date(2021, 1, 27); // 27 feb 2021
console.log(newDate); //Sat Feb 27 2021 00:00:00 GMT+1000 (Владивосток, стандартное время)

let year = newDate.getFullYear();
console.log(year); //2021

let month = newDate.getMonth();
console.log(month); // 1 (февраль)

let day = newDate.getDay();
console.log(day); //6

let time = newDate.getTime();
console.log(time); //1614348000000
