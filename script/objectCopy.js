// ********* КОНСПЕКТ УРОКА *********

// ПРИ КОПИРОВАНИИ КОПИРУЕТСЯ ЛИШЬ ССЫЛКА!
let user = {
	name: 'John',
	sizes: {
		height: 182,
		width: 50,
	}
};

let admin = user;
user.sizes.height = 200;

console.log(admin); // выводит height = 200;

/*********************************************************************** */

// ДВА ОБЪЕКТА РАВНЫ, ЕСЛИ ЭТО ОДИН И ТОТ ЖЕ ОБЪЕКТ

let a = {};
let b = a;

console.log( a == b); // true
console.log(a === b);// true

// ДВА ОБЪЕКТА НЕ РАВНЫ, ХОТЯ ОБА ПУСТЫ:

let c = {};
let d = {};

console.log( c == d); // false

/**********************************************************************/    

// Клонирование (Дублирование) объектов

let student = {
	name: "Adam",
	age: 30,
};

let clone = {};

for (let key in student) {
	clone[key] = student[key];
}
console.log(student);
console.log(clone);

clone.name = "Eve";
console.log(student);
console.log(clone);

//******************************************************************** */

// Object.assign 

// Объединяем несколько объектов

let teacher = { name: 'Noah'};

let permissions1 = { canView: true };
let permissions2 = { canEdit: true };

Object.assign(teacher, permissions1, permissions2);

console.log(teacher);

/******* */

// Перезаписываем свойство объекта

let child = { name: "Mary"};
console.log(child.name);
Object.assign(child, { name: "Martha"});
console.log(child.name);

/****** */

// ИСПОЛЬЗОВАНИЕ Object.assign для клонирования объекта

let worker = {
	name: 'Sim',
	age: 30,
};

console.log(worker);

let helper = Object.assign({}, worker);

console.log(worker);
console.log(helper);

