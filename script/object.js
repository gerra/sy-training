// ЗАДАЧА 1

/*
Напишите код, выполнив задание из каждого пункта отдельной строкой:
* Создайте пустой объект user.
* Добавьте свойство name со значением John.
* Добавьте свойство surname со значением Smith.
* Измените значение свойства name на Pete.
* Удалите свойство name из объекта.
*/

// РЕШЕНИЕ ЗАДАЧИ 1

console.log('ЗАДАЧА 1');

let user = {};
user.name = 'John';
user.surname = 'Smith';
console.log(user.name, user.surname);
user.name = 'Pete';
console.log(user.name);
delete user.name;
console.log(user.name);

//ЗАДАЧА 2

/*
Проверка на пустоту
Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств, иначе false.
Должно работать так:
let schedule = {};
alert( isEmpty(schedule) ); // true
schedule["8:30"] = "get up";
alert( isEmpty(schedule) ); // false
*/

// РЕШЕНИЕ ЗАДАЧИ 2
console.log('ЗАДАЧА 2');
// РЕШЕНИЕ ЗАДАЧИ ЧЕРЕЗ МЕТОД Object.keys()
let obj = {};
console.log(Object.keys(obj));
console.log(isEmpty(obj));
function isEmpty(obj) { 
    return Object.keys(obj) == ''
    ? true 
    : false;
    };

/* ПЕРВОНАЧАЛЬНОЕ РЕШЕНИЕ ЗАДАЧИ ЧЕРЕЗ ЦИКЛ.
function isEmpty(obj) {
    for (let key in obj) { // *Проверка на пустоту.
        return false;
    }
    return true;
} */

let schedule = {};
console.log( isEmpty(schedule) );
schedule["8:30"] = "get up";
console.log( isEmpty(schedule) );

// ЗАДАЧА 3
/*
Объекты-константы?
важность: 5
Можно ли изменить объект, объявленный с помощью const? Как вы думаете?
const user = {
  name: "John"
};
// это будет работать?
user.name = "Pete";
*/

console.log('ЗАДАЧА 3');
const users = {
    name: 'John'
};
users.name = 'Pete';
console.log(users.name);

// ЗАДАЧА 4
/*
Сумма свойств объекта
У нас есть объект, в котором хранятся зарплаты нашей команды:
let salaries = {
  John: 100,
  Ann: 160,
  Pete: 130
}
*/

// РЕШЕНИЕ ЗАДАЧИ 4

console.log('ЗАДАЧА 4');

let salaries = {
    John: 100,
    Ann: 160,
    Pete: 130
}
let sum = 0;  
for (let names in salaries) {
    sum += salaries[names];
}

console.log(sum);

// ЗАДАЧА 5

/*
Умножаем все числовые свойства на 2
Создайте функцию multiplyNumeric(obj), которая умножает все числовые свойства объекта obj на 2.
Например:

// до вызова функции
let menu = {
  width: 200,
  height: 300,
  title: "My menu"
};

multiplyNumeric(menu);

// после вызова функции
menu = {
  width: 400,
  height: 600,
  title: "My menu"
};
*/

// РЕШЕНИЕ ЗАДАЧИ 5

console.log('ЗАДАЧА 5');

let menu = {
    width: 200,
    height: 300,
    length: 50,
    title: "My menu",
};

console.log(menu);

function multiplyNumeric(menu) {
    for(let key in menu) {
        if (typeof menu[key] === 'number'){
            menu[key] = menu[key] * 2
        } 
        continue;
    }
}

multiplyNumeric(menu);

console.log (menu);