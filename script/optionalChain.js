//КОНСПЕКТ ЛЕКЦИИ

// Проблема "Несуществующего свойства"
let user = {};
console.log(user.address.street);

let html = document.querySelector('.my-element').innerHTML;

let user = {};
console.log(user?.address?.street);

//Сокращенное вычисление
let x = 0;
let user = {
    sayHi: function(x) {
        return x + 1;
    },
};
user.sayHi(x);
console.log(user.sayHi(x));

//ДРУГИЕ ВАРИАНТЫ ПРИМЕНЕНИЯ
let user1 = {
    admin() {
        console.log(`I'm administrator!`);
    },
}

let user2 = {};

user1.admin?.();
user2.admin?.();

//ИСПОЛЬЗОВАНИЕ КВАДРАТНЫХ СКОБОК
let user2 = {
    firstName: 'Ivan'
};
let user3 = null;
let key = 'firstName';

console.log(user2?.[key]);
console.log(user3?.[key]);
console.log(user2?.[key]?.something?.not?.existing);

// ГЛАВА ПРО СИМВОЛЫ

let id1 = Symbol('id');
let id2 = Symbol('id');
console.log(id1 === id2); // false
console.log(id1);

// Символы в летеральном объекте
let id3 = Symbol('id');
let user = {
    name: 'Вася',
    [id3]: 123,
};
console.log(user);
console.log(id3);

//Символы игнорируются циклом for...in
let id4 = Symbol('id');
let user = {
    name: 'Abraham',
    age: 80,
    [id4]: 123,
};
for (let key in user) console.log(key);

//Object.assign копирует и строки и символы
let id5 = Symbol('id');
let user5 = {
    [id5]: 123,
};
let clone = Object.assign({}, user5);
console.log(clone[id5]);

//ПРЕОБРАЗОВАНИЕ ОБЪЕКТОВ В ПРИМИТИВЫ (string, number, default)
let user6 = {
    name: "Isaak",
    money: 1000,

    [Symbol.toPrimitive] (hint) {
        console.log(`hint: ${hint}`);
        return hint == 'string'
            ? `{name: ${this.name}}`
            : this.money;
    }
};

console.log(user6);
console.log(+user6);
console.log(user6 + 500);

//МЕТОДЫ У ПРИМИТИВОВ

/*
7 примитивных методов:
string,
number,
boolean,
symbol,
null,
underfined,
bigint.
*/

//метод str.toUpperCase()

let str = 'привет!';
console.log(str.toUpperCase());
let a = 'пока!';
console.log(a.toUpperCase());

//Округление до n toFixed(n)
let n = 1.33455566;
console.log(n.toFixed(4)); // 1.3345

console.log(typeof 0);
console.log(typeof true);
console.log(typeof 'name');
console.log(typeof '33');
console.log(typeof undefined);
console.log(typeof Infinity);
console.log(typeof NaN);

//ЗАДАЧА
/*
Можно ли добавить свойство строке?
важность: 5
Взгляните на следующий код:
let str = "Привет";
str.test = 5;
alert(str.test);
Как вы думаете, это сработает? Что выведется на экран?
*/
let str = 'Hello!';
str.test = 5;
console.log(str.test); //undefined

