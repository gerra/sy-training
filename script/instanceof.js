// ЗАДАЧА 1

// Странный instanceof
// важность: 5
// Почему instanceof в примере ниже возвращает true? Мы же видим, что a не создан с помощью B().

// РЕШЕНИЕ ЗАДАЧИ 1
console.log('ЗАДАЧА 1');

function A() {}
function B() {}

A.prototype = B.prototype = {};

let a = new A();

console.log(a instanceof B); // true , т.к. идет проверка не функции, а прототипа.
console.log(a.__proto__ === B.prototype); // true 