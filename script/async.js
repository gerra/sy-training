// ЗАДАЧА 1

// Перепишите, используя async/await
// Перепишите один из примеров раздела Цепочка промисов, используя async/await вместо .then/catch:

// function loadJson(url) {
//   return fetch(url)
//     .then(response => {
//       if (response.status == 200) {
//         return response.json();
//       } else {
//         throw new Error(response.status);
//       }
//     })
// }

// loadJson('no-such-user.json') // (3)
//   .catch(console.log); // Error: 404

// РЕШЕНИЕ ЗАДАЧИ 1
console.log('ЗАДАЧА 1');

async function loadJson(url) {
    let response = await fetch(url);

    if (response.status === 200) {
        let json = await response.json();
        return json;
    } else {
        await Promise.reject(Error(response.status));
    }
}

loadJson('no-such-user.json') // (3)
    .catch(console.log); // Error: 404


// ЗАДАЧА 2

// Перепишите, используя async/await
// Ниже пример из раздела Цепочка промисов, перепишите его, используя async/await вместо .then/catch.

// В функции demoGithubUser замените рекурсию на цикл: используя async/await, сделать это будет просто.

// class HttpError extends Error {
//   constructor(response) {
//     super(`${response.status} for ${response.url}`);
//     this.name = 'HttpError';
//     this.response = response;
//   }
// }

// function loadJson(url) {
//   return fetch(url)
//     .then(response => {
//       if (response.status == 200) {
//         return response.json();
//       } else {
//         throw new HttpError(response);
//       }
//     })
// }

// // Запрашивать логин, пока github не вернёт существующего пользователя.
// function demoGithubUser() {
//   let name = prompt('Введите логин?', 'iliakan');

//   return loadJson(`https://api.github.com/users/${name}`)
//     .then(user => {
//       console.log(`Полное имя: ${user.name}.`);
//       return user;
//     })
//     .catch(err => {
//       if (err instanceof HttpError && err.response.status == 404) {
//         console.log('Такого пользователя не существует, пожалуйста, повторите ввод.');
//         return demoGithubUser();
//       } else {
//         throw err;
//       }
//     });
// }

// demoGithubUser();


// РЕШЕНИЕ ЗАДАЧИ 2
console.log('ЗАДАЧА 2');

class HttpError extends Error {
    constructor(response) {
        super(`${response.status} for ${response.url}`);
        this.name = 'HttpError';
        this.response = response;
    }
}

async function loadJson(url) {
    let response = await fetch(url);

    if (response.status === 200) {
        let json = await response.json();
        return json;
    } else {
        return await Promise.reject(HttpError(response.status));
    }
}

// Запрашивать логин, пока github не вернёт существующего пользователя.
async function demoGithubUser() {
    let name = prompt('Введите логин?', 'iliakan');

    let user = await loadJson(`https://api.github.com/users/${name}`)
        .catch(err => { 
            if (err instanceof HttpError && err.response.status === 404) {
                console.log('Такого пользователя не существует, пожалуйста, повторите ввод.');
                return demoGithubUser();
            } else {
                throw err;
            }
        });

    console.log(`Полное имя: ${user.name}`);
    return user;
}

demoGithubUser();


// ЗАДАЧА 3

// Вызовите async–функцию из "обычной"
// Есть «обычная» функция. Как можно внутри неё получить результат выполнения async–функции?

// async function wait() {
//   await new Promise(resolve => setTimeout(resolve, 1000));

//   return 10;
// }

// function f() {
//   // ...что здесь написать?
//   // чтобы вызвать wait() и дождаться результата "10" от async–функции
//   // не забывайте, здесь нельзя использовать "await"
// }
// P.S. Технически задача очень простая, но этот вопрос часто задают разработчики, недавно познакомившиеся с async/await.


// РЕШЕНИЕ ЗАДАЧИ 3
console.log('ЗАДАЧА 3');

async function wait() {
    await new Promise(resolve => setTimeout(resolve, 3000));

    return 10;
}
/**
 * вызываем функцию, которая через .then выполняет вывод результата.
 */
function f() {
    return wait().then(result => console.log(result));
}

f();
