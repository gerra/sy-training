// ЗАДАЧА 1

/*
Перепишите с использованием функции-стрелки
Замените код Function Expression стрелочной функцией:

function ask(question, yes, no) {
  if (confirm(question)) yes()
  else no();
}

ask(
  "Вы согласны?",
  function() 
  	{ alert("Вы согласились."); },
  function() 
  	{ alert("Вы отменили выполнение."); }
);
*/

//РЕШЕНИЕ ЗАДАЧИ 1
console.log('ЗАДАЧА 1');
let ask = (confirm('Вы согалсны?'))
	? () => console.log('Вы согласились')
	: () => console.log('Вы отменили выполнение');

	ask();
