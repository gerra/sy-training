// КОНСПЕКТ 

// НАСЛЕДОВАНИЕ КЛАССОВ

class Animal {
    constructor(name) { // прописываем здесь все ключи и их значения.
        this.speed = 0;
        this.name = name;
    }

    run(speed) { // добавляем метод run
        this.speed = speed;
        console.log(`${this.name} бежит со скоростью ${this.speed} км/ч.`);
    }

    stop() {
        this.speed = 0;
        console.log(`${this.name} стоит.`);
    }
}

class Rabbit extends Animal {
    hide() {
        console.log(`${this.name} прячется!`);
    }

    stop() {
        super.stop();
        this.hide();
    }
}

let rabbit = new Rabbit('Белый кролик');

rabbit.run(6);
rabbit.stop();

