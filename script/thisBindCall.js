// ***********КОНСПЕКТ ВИДЕО УРОКОВ ****************
// ВЛАДИЛЕН МИНИН. ЧТО ТАКОЕ КОНТЕКСТ THIS. КАК РАБОТАЕТ CALL, BIND, APPLY

function hello() {
    console.log('Hello', this)
}

const person = {
    name: 'Mattew',
    age: 33,
    sayHello: hello,
//	sayHelloWindow: hello.bind(document),
    logInfo: function(job, phone) {
        console.group(`${this.name} info:`)
        console.log(`Name is ${this.name}`)
        console.log(`Age is ${this.age}`)
        console.log(`Job is ${job}`)
        console.log(`Phone is ${phone}`)
        console.groupEnd()
    }
}

const luke = {
    name: 'Luke',
    age: 44,
}

// const lukeInfoLog = person.logInfo.bind(luke, 'FrontEnd', '8-888-888-88-88'); 

// Метод bind не вызывает функцию. Он возвращает заданную в скобках функцию

//lukeInfoLog()

person.logInfo.bind(luke, 'FrontEnd', '8-888-888-88-88')()
person.logInfo.call(luke, 'FrontEnd', '8-999-999-99-99')
person.logInfo.apply(luke, ['FrontEnd', '8-999-999-99-99'])


// Создаем функцию для перемножения каждого элемента массива на заданное число.



/*function multBy(arr, n) {
    return arr.map(function(i) {
        return i * n
    })
} 

console.log(multBy(array, 10)) 
*/
/* Array.prototype.multBy = function(n) {
    return this.map(function(i) {
        return i * n;
    })
} */
const array = [1, 2, 3, 4, 5]

Array.prototype.multiply = function(n) {
	return this.map( (i) => i * n );
}

console.log(array.multiply(5)); 

/************ */
/************ */
/************ */
/************ */

// АЛЕКС ЛУЩЕНКО. THIS в JS.
console.log('МЕНЯЕМ ЦВЕТ ФОНА ПО КЛИКУ НА ПАРАГРАФ');

function abc() {
    this.style.background === 'rgb(94, 94, 94)'
        ? this.style.background = 'black'
        : this.style.background = 'rgb(94, 94, 94)'
};

//document.querySelector('div').onclick = abc; 

let p = document.querySelectorAll('div');
p.forEach(function(element){
    element.onclick = abc;
})

