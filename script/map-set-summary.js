// SUMMARY (КОНСПЕКТ)

// MAP
// map - это коллекция ключ/значение

// new Map() – создаёт коллекцию.
// map.set(key, value) – записывает по ключу key значение value.
// map.get(key) – возвращает значение по ключу или undefined, если ключ key отсутствует.
// map.has(key) – возвращает true, если ключ key присутствует в коллекции, иначе false.
// map.delete(key) – удаляет элемент по ключу key.
// map.clear() – очищает коллекцию от всех элементов.
// map.size – возвращает текущее количество элементов.

// Перебор Map

let newMap = new Map([
	['tomato', 100],
	['cucumber', 200],
	['onion', 300]
])
console.log({newMap});

// Перебор по ключам:
for (let vegetables of newMap.keys()){
	console.log(vegetables);
}

// Перебор по значениям:
for (let amount of newMap.values()){
	console.log(amount);
}

// Перебор по ключ/значение:
for (let entry of newMap.entries()){
	console.log(entry);
}

let newArr = []; // (*) создаем новый массив

// Встроенный метод forEach:
newMap.forEach((value, key, map) => {
	newArr.push(value * 10); // (*) закидываем в новый массив значения
	this.yourFunc(value * 10);
	console.log(`${key} / 10 = ${value / 10}`);
})

console.log({newArr});

function yourFunc(param) {
	console.log(param);
}

//Object.entries: Map из Object
console.log('Object.entries: Map из Object')
let firstMap = new Map([
	['1', 'str1'],
	[1, 'num1'],
	[true, 'bool1']
]);
console.log(firstMap.get(1));
console.log(firstMap.get('1'));
console.log(firstMap.get(true));

// КАК ИЗ ОБЪЕКТА СОЗДАТЬ map:
// создаем объект:
console.log('создаем объект из map')
let obj = {
	banana: 15,
	apple: 20,
	pineapple: 35,
}
console.log({obj});
// создаем secondMap из объекта obj:
let secondMap = new Map(Object.entries(obj));
console.log({secondMap});

// КАК ИЗ MAP СОЗДАТЬ ОБЪЕКТ?
// создаем map:
let thirdMap = new Map();
thirdMap.set('guitar', 100);
thirdMap.set('keys', 200);
thirdMap.set('bass', 300);

console.log({thirdMap});

let thirdObj = Object.fromEntries(thirdMap.entries());
console.log({thirdObj});

// Как из MAP создать массив?

let fourMap = new Map();
fourMap.set('mon', 10);
fourMap.set('tue', 20);
fourMap.set('wed', 30);

let keyArr = Array.from(fourMap.keys());
console.log({keyArr});
let valueArr = Array.from(fourMap.values());
console.log({valueArr});