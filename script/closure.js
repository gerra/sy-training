// ЗАДАЧА 1
// Независимы ли счётчики?
// важность: 5
// Здесь мы делаем два счётчика: counter и counter2, используя одну и ту же функцию makeCounter.

// Они независимы? Что покажет второй счётчик? 0,1 или 2,3 или что-то ещё?

// function makeCounter() {
//   let count = 0;

//   return function() {
//     return count++;
//   };
// }
// let counter = makeCounter();
// let counter2 = makeCounter();
// alert( counter() ); // 0
// alert( counter() ); // 1
// alert( counter2() ); // ?
// alert( counter2() ); // ?

// РЕШЕНИЕ ЗАДАЧИ 1
console.log('ЗАДАЧА 1');

function makeCounter() {
    let count = 0;
    return function() {
        return count++;
  };
}
let counter = makeCounter();
let counter2 = makeCounter();
console.log( counter() ); // 0
console.log( counter() ); // 1
console.log( counter2() ); // 0
console.log( counter2() ); // 1

// ЗАДАЧА 2

// Объект счётчика
// важность: 5
// Здесь объект счётчика создан с помощью функции-конструктора.

// Будет ли он работать? Что покажет?

// РЕШЕНИЕ ЗАДАЧИ 2.
console.log('ЗАДАЧА 2');

function Counter() {
    let count = 0;
    this.up = function() {
        return ++count;
  };
    this.down = function() {
        return --count;
  };
}
let newCounter = new Counter();
console.log( newCounter.up() ); // 1
console.log( newCounter.up() ); // 2
console.log( newCounter.down() ); // 1

// ЗАДАЧА 3

// Функция в if
// Посмотрите на код. Какой будет результат у вызова на последней строке?

// РЕШЕНИЕ ЗАДАЧИ 3
console.log('ЗАДАЧА 3');

let phrase = "Hello";
if (true) {
    let user = "John";

    function sayHi() {
        console.log(`${phrase}, ${user}`);
  }
}
sayHi(); // Hello, John

// ЗАДАЧА 4
// Сумма с помощью замыканий
// важность: 4
// Напишите функцию sum, которая работает таким образом: sum(a)(b) = a+b.
// Да, именно таким образом, используя двойные круглые скобки (не опечатка).
// Например:
// sum(1)(2) = 3
// sum(5)(-1) = 4

// РЕШЕНИЕ ЗАДАЧИ 4
console.log('ЗАДАЧА 4');

function sum(a) {
    return b => a + b;
}
console.log(sum(4)(4)); // 8
console.log(sum(5)(5)); // 10
console.log(sum(100)(500)); // 600

//ЗАДАЧА 5

// Фильтрация с помощью функции
// важность: 5
// У нас есть встроенный метод arr.filter(f) для массивов. Он фильтрует все элементы с помощью функции f. Если она возвращает true, то элемент добавится в возвращаемый массив.
// Сделайте набор «готовых к употреблению» фильтров:
// inBetween(a, b) – между a и b (включительно).
// inArray([...]) – находится в данном массиве.
// Они должны использоваться таким образом:

// arr.filter(inBetween(3,6)) – выбирает только значения между 3 и 6 (включительно).
// arr.filter(inArray([1,2,3])) – выбирает только элементы, совпадающие с одним из элементов массива
// Например:
// /* .. ваш код для inBetween и inArray */
// let arr = [1, 2, 3, 4, 5, 6, 7];
// alert( arr.filter(inBetween(3, 6)) ); // 3,4,5,6
// alert( arr.filter(inArray([1, 2, 10])) ); // 1,2

// РЕШЕНИЕ ЗАДАЧИ 5
console.log('ЗАДАЧА 5');

function inBetween(c, d) {
    return x => x >= c && x <= d;
}

function inArray(arr) {
    return x => arr.includes(x);
}

let arr = [1, 2, 3, 4, 5, 6, 7];
console.log( arr.filter(inBetween(3, 6)) ); // [3, 4, 5, 6]
console.log( arr.filter(inArray([1, 2, 10])) ); // [1, 2]

// ЗАДАЧА 6
// Сортировать по полю
// важность: 5
// У нас есть массив объектов, который нужно отсортировать:
// let users = [
//   { name: "John", age: 20, surname: "Johnson" },
//   { name: "Pete", age: 18, surname: "Peterson" },
//   { name: "Ann", age: 19, surname: "Hathaway" }
// ];
// Обычный способ был бы таким:
// // по имени (Ann, John, Pete)
// users.sort((a, b) => a.name > b.name ? 1 : -1);

// // по возрасту (Pete, Ann, John)
// users.sort((a, b) => a.age > b.age ? 1 : -1);
// Можем ли мы сделать его короче, скажем, вот таким?

// users.sort(byField('name'));
// users.sort(byField('age'));
// То есть, чтобы вместо функции, мы просто писали byField(fieldName).

// Напишите функцию byField, которая может быть использована для этого.

// РЕШЕНИЕ ЗАДАЧИ 6
console.log('ЗАДАЧА 6');

let users = [
    { name: "John", age: 20, surname: "Johnson" },
    { name: "Pete", age: 18, surname: "Peterson" },
    { name: "Ann", age: 19, surname: "Hathaway" },
    { name: "Luke", age: 50, surname: "Skywalker"},
];
function byField(key) {
    return (first, second) => first[key] > second[key] ? 1 : -1;
}

console.log(users.sort(byField('name')));
users.forEach(user => console.log(user.name)); // Ann, John, Luke, Pete
console.log(users.sort(byField('age')));
users.forEach(user => console.log(user.name)); // Pete, Ann, John, Luke

// ЗАДАЧА 7
// Армия функций
// важность: 5
// Следующий код создаёт массив из стрелков (shooters).
// Каждая функция предназначена выводить их порядковые номера. Но что-то пошло не так…
// function makeArmy() {
//   let shooters = [];
//   let i = 0;
//   while (i < 10) {
//     let shooter = function() { // функция shooter
//       alert( i ); // должна выводить порядковый номер
//     };
//     shooters.push(shooter);
//     i++;
//   }
//   return shooters;
// }
// let army = makeArmy();
// army[0](); // у 0-го стрелка будет номер 10
// army[5](); // и у 5-го стрелка тоже будет номер 10
// // ... у всех стрелков будет номер 10, вместо 0, 1, 2, 3...
// Почему у всех стрелков одинаковые номера? Почините код, чтобы он работал как задумано.

// РЕШЕНИЕ ЗАДАЧИ 7
console.log('ЗАДАЧА 7');

function makeArmy() {
    let shooters = [];
    console.log({shooters});
    for (let i = 0; i < 10; i++) {
        let shooter = () => console.log(i);
        shooters.push(shooter);
  }
    return shooters;
}
let army = makeArmy();
army[0](); // 0
army[5](); // 5

