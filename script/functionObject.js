// ЗАДАЧА 1

// Установка и уменьшение значения счётчика
// важность: 5
// Измените код makeCounter() так, чтобы счётчик мог увеличивать и устанавливать значение:

// counter() должен возвращать следующее значение (как и раньше).
// counter.set(value) должен устанавливать счётчику значение value.
// counter.decrease() должен уменьшать значение счётчика на 1.
// Посмотрите код из песочницы с полным примером использования.

// P.S. Для того, чтобы сохранить текущее значение счётчика, можно воспользоваться как замыканием, так и свойством функции. Или сделать два варианта решения: и так, и так.

// РЕШЕНИЕ ЗАДАЧИ 1
console.log('ЗАДАЧА 1');
function makeCounter() {
    let count = 0;
    function counter() {
        return count += 1;
    }
    counter.set = value => count = value;
    counter.decrease = () => count -= 1;
    return counter;
}

let counter = new makeCounter();

console.log(counter()); // 1
console.log(counter()); // 2
console.log(counter()); // 3

console.log(counter.set(100)); // 100

console.log(counter.decrease()); // 99
console.log(counter.decrease()); // 98
console.log(counter.decrease()); // 97
console.log(counter.decrease()); // 96
console.log(counter.decrease()); // 95

console.log(counter()); // 96
console.log(counter()); // 97
console.log(counter()); // 98
console.log(counter()); // 99
console.log(counter()); // 100

// ЗАДАЧА 2

// Сумма с произвольным количеством скобок
// важность: 2
// Напишите функцию sum, которая бы работала следующим образом:

// sum(1)(2) == 3; // 1 + 2
// sum(1)(2)(3) == 6; // 1 + 2 + 3
// sum(5)(-1)(2) == 6
// sum(6)(-1)(-2)(-3) == 0
// sum(0)(1)(2)(3)(4)(5) == 15
// P.S. Подсказка: возможно вам стоит сделать особый метод преобразования в примитив для функции.

// РЕШЕНИЕ ЗАДАЧИ 2
console.log('ЗАДАЧА 2');

function sum(a) {
    let result = a;

    function func(b) {
        result += b;
        return func;
    }

    func.toString = () => result;

    return func;
}

console.log(sum(5)(5)(5)); // 15
console.log(sum(10)(10)(10)); // 30



