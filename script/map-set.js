//ЗАДАЧИ ПО ТЕМЕ:

// ЗАДАЧА 1
// Фильтрация уникальных элементов массива
// важность: 5
// Допустим, у нас есть массив arr.
// Создайте функцию unique(arr), которая вернёт массив уникальных, не повторяющихся значений массива arr.
// Например:
// function unique(arr) {
//   /* ваш код */
// }
// let values = ["Hare", "Krishna", "Hare", "Krishna",
//   "Krishna", "Krishna", "Hare", "Hare", ":-O"
// ];
// alert( unique(values) ); // Hare,Krishna,:-O

// P.S. Здесь мы используем строки, но значения могут быть любого типа.
// P.P.S. Используйте Set для хранения уникальных значений.

// РЕШЕНИЕ ЗАДАЧИ 1:
console.log('ЗАДАЧА 1');

function unique(arr) {
    let newArr = [];
    let set = new Set(Object.values(arr));
    set.forEach((value) => newArr.push(value))
    return newArr;
}

let values = ['Хабаровск', 'Биробиджан', 'Комсомольск-на-Амуре', 'Хабаровск', 'Биробиджан', 'Комсомольск-на-Амуре', 'Хабаровск', 'Биробиджан', 'Комсомольск-на-Амуре',];
console.log(unique(values));

// ЗАДАЧА 2

/* Отфильтруйте анаграммы
важность: 4
Анаграммы – это слова, у которых те же буквы в том же количестве, но они располагаются в другом порядке.
Например:
nap - pan
ear - are - era
cheaters - hectares - teachers
Напишите функцию aclean(arr), которая возвращает массив слов, очищенный от анаграмм.
Например:
let arr = ["nap", "teachers", "cheaters", "PAN", "ear", "era", "hectares"];
alert( aclean(arr) ); // "nap,teachers,ear" or "PAN,cheaters,era"
Из каждой группы анаграмм должно остаться только одно слово, не важно какое. */

// РЕШЕНИЕ ЗАДАЧИ 2
console.log('ЗАДАЧА 2');
function aclean(arr) {
    let map = new Map();
    let sortItem;
    arr.forEach((item, index) => {
        sortItem = item.toLowerCase().split('').sort().join(''),
        map.set(sortItem, arr[index]);
    });
    let newArr = [];
    map.forEach((value, key, map) => newArr.push(value));
    return newArr;
}
let arr = ["nap", "teachers", "cheaters", "PAN", "ear", "era", "hectares"];
console.log(aclean(arr));

// ЗАДАЧА 3

/* Перебираемые ключи
важность: 5
Мы хотели бы получить массив ключей map.keys() в переменную и далее работать с ними, например, применить метод .push.
Но это не выходит:
let map = new Map();
map.set("name", "John");
let keys = map.keys();
// Error: keys.push is not a function
// Ошибка: keys.push -- это не функция
keys.push("more");
Почему? Что нужно поправить в коде, чтобы вызов keys.push сработал? */

// РЕШЕНИЕ ЗАДАЧИ 3
console.log('ЗАДАЧА 3');

let map = new Map();
map.set("name", "John");
map.set("second name", "Piper");
map.set("age", "60");

let keys = Array.from(map.keys());

keys.push('phone');
console.log({keys});