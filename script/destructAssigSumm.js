// КОНСПЕКТ

let arr = ['Abraham', 'Isaac', 'Jakob']; // Создаем массив arr
let [first, second, third] = arr; // Деструктурируем массив
// first = arr[0];
// second = arr[1];
// third = arr[3];
console.log(first); // Abraham
console.log(second); // Isaac
console.log(third); // Jacob

// РАБОТА СО SPLIT:

let [firstName, surname] = 'John Smith'.split(' ');
console.log(firstName);
console.log(surname);

// ВАРИАНТЫ ПЕРЕБОРА

let [a, b, c] = "abc";
let [one, two, three] = new Set([1, 2, 3]);
console.log (a);
console.log (b);
console.log (c);
console.log (one);
console.log (two);
console.log (three);

// ОСТАТОЧНЫЕ ДАННЫЕ ...

let [name1, name2, ...rest] = ["Julius", "Caesar", "Consul", "of the Roman Republic"];

alert(name1); // Julius
alert(name2); // Caesar

// Обратите внимание, что `rest` является массивом.
alert(rest[0]); // Consul
alert(rest[1]); // of the Roman Republic
alert(rest.length); // 2


