// ЗАДАЧА 1

// Работа с прототипами
// важность: 5
// В приведённом ниже коде создаются и изменяются два объекта.

// Какие значения показываются в процессе выполнения кода?

// let animal = {
//   jumps: null
// };
// let rabbit = {
//   __proto__: animal,
//   jumps: true
// };
// alert( rabbit.jumps ); // ? (1)
// delete rabbit.jumps;
// alert( rabbit.jumps ); // ? (2)
// delete animal.jumps;
// alert( rabbit.jumps ); // ? (3)

// РЕШЕНИЕ ЗАДАЧИ 1

console.log('ЗАДАЧА 1');

let animal = {
    jumps: null
};

let rabbit = {
    __proto__: animal,
    jumps: true
};

console.log(rabbit.jumps); // true

delete rabbit.jumps;

console.log(rabbit.jumps); // null

delete animal.jumps;

console.log(rabbit.jumps); // undefined


// ЗАДАЧА 2

// Алгоритм поиска
// важность: 5
// Задача состоит из двух частей.

// У нас есть объекты:

// let head = {
//   glasses: 1
// };

// let table = {
//   pen: 3
// };

// let bed = {
//   sheet: 1,
//   pillow: 2
// };

// let pockets = {
//   money: 2000
// };
// С помощью свойства __proto__ задайте прототипы так, чтобы поиск любого свойства выполнялся по следующему пути: pockets → bed → table → head. Например, pockets.pen должно возвращать значение 3 (найденное в table), а bed.glasses – значение 1 (найденное в head).
// Ответьте на вопрос: как быстрее получить значение glasses – через pockets.glasses или через head.glasses? При необходимости составьте цепочки поиска и сравните их.

// РЕШЕНИЕ ЗАДАЧИ 2

console.log('ЗАДАЧА 2')

let head = {
    glasses: 1
};

let table = {
    pen: 3,
    __proto__: head,
};

let bed = {
    sheet: 1,
    pillow: 2,
    __proto__: table,
};

let pockets = {
    money: 2000,
    __proto__: bed,
};

for (let key in pockets) 
    console.log(key); // money, sheet, pillow, pen, glasses

for (let key in pockets) 
    console.log(pockets[key]); // 2000, 1, 2, 3, 1

console.log(pockets.pen); // 3 // Взято из table

console.log(bed.glasses); // 1 // Взято из head

console.log(pockets.glasses); // 1

console.log(head.glasses); // 1


// ЗАДАЧА 3

// Куда будет произведена запись?
// важность: 5
// Объект rabbit наследует от объекта animal.

// Какой объект получит свойство full при вызове rabbit.eat(): animal или rabbit?

// let animal = {
//   eat() {
//     this.full = true;
//   }
// };

// let rabbit = {
//   __proto__: animal
// };

// rabbit.eat();

// РЕШЕНИЕ ЗАДАЧИ 3

console.log('ЗАДАЧА 3');

let animalZoo = {
    eat() {
        this.full = true;
    }
};

let rabbitZoo = {
    __proto__: animalZoo
};

console.log(rabbitZoo.eat()); // undefined // потому что используется объект rabbitZoo

// ЗАДАЧА 4

// Почему наедаются оба хомяка?
// важность: 5
// У нас есть два хомяка: шустрый (speedy) и ленивый (lazy); оба наследуют от общего объекта hamster.

// Когда мы кормим одного хомяка, второй тоже наедается. Почему? Как это исправить?

// let hamster = {
//   stomach: [],

//   eat(food) {
//     this.stomach.push(food);
//   }
// };

// let speedy = {
//   __proto__: hamster
// };

// let lazy = {
//   __proto__: hamster
// };

// // Этот хомяк нашёл еду
// speedy.eat("apple");
// alert( speedy.stomach ); // apple

// // У этого хомяка тоже есть еда. Почему? Исправьте
// alert( lazy.stomach ); // apple

// РЕШЕНИЕ ЗАДАЧИ 4

console.log('ЗАДАЧА 4');

let hamster = {
    stomach: [],

    eat(food) {
        this.stomach.push(food);
    }
};

let speedy = {
    stomach: [],

    eat(food) {
        this.stomach.push(food);
    },

    __proto__: hamster,
};

let lazy = {
    __proto__: hamster,
};

// Этот хомяк нашёл еду
speedy.eat("apple");
console.log(speedy.stomach); // apple

console.log(lazy.stomach); // []

