// КОНСПЕКТ (SUMMARY)

// ПЕРВОЕ. 
// Метод Symbol.itetator

let range = {
    from: 1,
    to: 5
};
console.log(range);

// Вот так итерация (перебор) работать не будет:
/* for (let num of range)
console.log(num)
 */ 
/* Чтобы сделать range итерируемым (и позволить for..of работать с ним), 
нам нужно добавить в объект метод с именем Symbol.iterator 
(специальный встроенный Symbol, созданный как раз для этого). */

range[Symbol.iterator] = function() {
    return {
        current: this.from,
        last: this.to,
        next() {
            if (this.current <= this.last) {
                return {
                    done: false,
                    value: this.current++,
                } 
            } else 
                return {done: true}
        }
    }
}
console.log({range}); 

for (let num of range) {
    console.log(num);
}

// ВТОРОЕ
// Перебор строки

let str = 'test';
for (let char of str) {
    console.log(char)
}

// Перебор массива
let arr = [1, 2, 3, 4]
console.log({arr});
for (let index of arr) {
    console.log(index)
}

// ТРЕТЬЕ
// Явный выбор итератора
let str1 = 'Joy!';
console.log(str1);
let iterator = str1[Symbol.iterator]();
console.log(iterator);

while (true) {
    let result = iterator.next();
    if (result.done) break;
    console.log(result.value);
}

// ЧЕТВЕРТОЕ
// Объекты итерировать нельзя! Для этого нужно использовать либо метод Symbol.iterator, либо...

// Array.from()

let arrayLike = {
    0: 'Hello',
    1: 'World',
    2: '!',
    length: 3,
}
console.log({arrayLike});
// Вот так работать не будет!
/* for (let index1 of arrayLike) {
    console.log(index1);
} */
let arr0 = Array.from(arrayLike);
for (let index1 of arr0) {
    console.log(index1);
}

let arr1 = Array.from(range);
console.log({arr1});
console.log(arr1);

// В Array.from можно указать изменяющую функцию.
let newRange = {
    from: 5,
    to: 10
};
console.log({newRange})

newRange[Symbol.iterator] = function() {
    return {
        current: this.from,
        last: this.to,
        next() {
            if (this.current <= this.last) {
                return {
                    done: false,
                    value: this.current++,
                } 
            } else 
                return {done: true}
        }
    }
}

let arr2 = Array.from(newRange, num => num * 10);
console.log({arr2});
