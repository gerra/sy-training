// ЗАДАЧА 1
// Напишите "if", аналогичный "switch"

/*
Напишите if..else, соответствующий следующему switch:
switch (browser) {
  case 'Edge':
    alert( "You've got the Edge!" );
    break;

  case 'Chrome':
  case 'Firefox':
  case 'Safari':
  case 'Opera':
    alert( 'Okay we support these browsers too' );
    break;

  default:
    alert( 'We hope that this page looks ok!' );
}
*/

// РЕШЕНИЕ ЗАДАЧИ 1

console.log('ЗАДАЧА 1');
let browser = prompt('Каким браузером вы пользуетесь?', 'Edge');
if (browser === null) {
    console.log('Отмена.');
} else 
    if (browser === 'Chrome' || browser === 'Firefox' || browser === 'Safari' || browser === 'Opera') {
        console.log('Окей, этот браузер тоже поддерживается.');
    } else 
        if (browser === 'Edge') {
            console.log('Вау! Вы пользуетесь браузером Edge!')
        } else {
            console.log('Неизвестный браузер. Мы надеемся, что ваша страница все равно отображается верно.');
        }

// ЗАДАЧА 2
// Переписать условия "if" на "switch"

/* 
Перепишите код с использованием одной конструкции switch:
const number = +prompt('Введите число между 0 и 3', '');
if (number === 0) {
  alert('Вы ввели число 0');
}
if (number === 1) {
  alert('Вы ввели число 1');
}
if (number === 2 || number === 3) {
  alert('Вы ввели число 2, а может и 3');
}
*/

// РЕШЕНИЕ ЗАДАЧИ 2

console.log('ЗАДАЧА 2');
const number = prompt('Введите число между 0 и 3', '');
if (number === null) {
    console.log('Отмена.');
} else 
    if (number < 0 || number > 3) {
    console.log('Ошибка. Вы ввели неверное число.');
    } else 
        switch (+number) {
            case 0 :
                console.log('Вы ввели число 0!');
                break;
            case 1 :
                console.log('Вы ввели число 1!');
                break;
            case 2 :
            case 3 :
                console.log('Вы ввели число 2, а может и 3');
                break;
        }
        