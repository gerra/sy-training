// ********** КОНСПЕКТ УРОКА **********

// ПРИМЕРЫ МЕТОДОВ

let user = {
    name: "John",
    age: 30,
};

user.sayHi = function() {
    console.log("Hi!");
};

user.sayHi(); // Hi!

// Можно объявить функцию заранее и использовать ее в качестве метода.

/* let user = {};
 */
function sayHi() {
    console.log("Hi!");
};

user.sayHi = sayHi;

user.sayHi(); // Hi!


// БОЛЕЕ КОРОТКИЙ СИНТАКСИС

user = {
    sayHi: function() {
        console.log("Hi!");
    }
};
user.sayHi();

//ЕЩЕ БОЛЕЕ КОРОТКИЙ СИНТАКСИС

user = {
    sayHi() {
        console.log("Hi!");
    }
};
user.sayHi();


/***************************************************** */

// КЛЮЧЕВОЕ СЛОВО THIS

 user = {
    name: "Abel",
    sayHi() {
        console.log(`Hello, ${this.name}!`);
    }
}

user.sayHi();

//ЗНАЧЕНИЕ THIS ЗАВИСИТ ОТ КОНТЕКСТА

 user = { 
    name: "Cain"
};
let admin = {
    name: "Lameh"
};

function sayHi() {
    console.log(`Hi, ${this.name}!`);
}

user.f = sayHi;
admin.f = sayHi;

user.f();
admin.f();
admin['f']();

/***************************************** */
// ВНУТРЕННЯЯ РЕАЛИЗАЦИЯ

// ПОТЕРЯ ЗНАЧЕНИЯ THIS

 user = {
    name: "John",
    hi() {
        console.log(`hi, ${this.name}!`)
    },
    bye() {
        console.log("Bye!") 
    },
};

user.hi();

//(user.name == 'John') ? user.hi : user.bye)(); // ОШИБКА!!!



/********************************* */
/********************************* */

//  ************ ЗАДАЧИ *********

// ЗАДАЧА 1

/*  
Проверка синтаксиса
Каким будет результат выполнения этого кода?
*/
console.log('ЗАДАЧА 1');
 user = {
    name: "John",
    go() {
        console.log(this.name);
    }
}; // Если пропустить точку с запятой, то будет ошибка.
(user.go)();


/************************ */

// ЗАДАЧА 2
/*
Объясните значение "this"
важность: 3
В представленном ниже коде мы намерены вызвать obj.go() метод 4 раза подряд.

Но вызовы (1) и (2) работают иначе, чем (3) и (4). Почему?

let obj, method;

obj = {
  go: function() { console.log(this); }
};

obj.go();               // (1) [object Object]

(obj.go)();

(method = obj.go)();    // (3) undefined

(obj.go || obj.stop)();

*/

/************************ */

//ЗАДАЧА 3

/**
 * СОЗДАЙТЕ КАЛЬКУЛЯТОР
 * Создайте объект calculator (калькулятор) с тремя методами:

read() (читать) запрашивает два значения и сохраняет их как свойства объекта.
sum() (суммировать) возвращает сумму сохранённых значений.
mul() (умножить) перемножает сохранённые значения и возвращает результат.
 * 
 */

// РЕШЕНИЕ ЗАДАЧИ 3

console.log("ЗАДАЧА 3");
const calculator = {
    read(a, b) {
         this.a = 5;
         this.b = 6;
    },
    sum() {
        return +this.a + +this.b;
    },
    mul() {
        return this.a * this.b;
    }
}

calculator.read();
console.log( calculator.sum() );
console.log( calculator.mul() );

// ЗАДАЧА 4

/***
 Цепь вызовов
Измените код методов up, down и showStep таким образом, чтобы их вызов можно было сделать по цепочке, например так:
ladder.up().up().down().showStep(); // 1
 */
console.log('ЗАДАЧА 4');
let ladder = {
    step: 0,
    up() {
        this.step++;
        return this;
    },
    down() {
        this.step--;
        return this;
    },
    showStep() {
        console.log(this.step);
        return this;
    }
}

ladder.up().up().up().down().showStep();


