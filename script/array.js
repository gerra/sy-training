// МАССИВЫ

//Объявление массивов
let arr = new Array();
let arr0 = [];

let fruits = ['Apple', 'Orange', 'plum'];
console.log(fruits[0]);
console.log(fruits[1]);
console.log(fruits[2]);

fruits[2] = 'pear'; //Замена элемента 2
console.log(fruits[0]);
console.log(fruits[1]);
console.log(fruits[2]);

fruits[3] = 'Lemon';// Добавили элемент 3 в массив
console.log(fruits[0]);
console.log(fruits[1]);
console.log(fruits[2]);
console.log(fruits[3]);

console.log(fruits.length);// Длина массива 4

console.log(fruits); // массив целиком [ 'Apple', 'Orange', 'pear', 'Lemon' ]

//методы, работающие с концом массива
//POP удаляет последний элемент и возвращает его
console.log(fruits.pop()); // Lemon
console.log(fruits);

//PUSH добавляет элемент в конец массива
fruits.push('Lemon');
console.log(fruits);

// SHIFT, UNSHIFT удаляет/добавляет элемент в начало массива
console.log(fruits.shift());//Apple
console.log(fruits);

fruits.unshift('Apple');
console.log(fruits);

//Методы PUSH и UNSHIFT могут добавлять по несколько элементов
let fruits00 = ['Apple'];
fruits00.push('Orange', 'pear');
fruits00.unshift('Pineapple', 'Lemon');
console.log(fruits00);

//Массивы ведут себя как объекты
let fruits0 = ['Banana'];
let arr00 = fruits0;// КОПИРОВАНИЕ
console.log(arr0 === fruits0); //true
arr00.push('Pear'); // массив меняется по ссылке.
console.log(fruits0); //Banana, Pear

//НЕПРАВИЛЬНОЕ ПРИМЕНЕНИЕ МАССИВОВ
// Добавление нечислового свойства
//arr1.test = 2;
// Создание дыр
arr[0], arr[33];
// Заполнение массива в обратном порядке
arr[1000], arr[999]

// ЭФФЕКТИВНОСТЬ
// Методы PUSH/POP выполннятются быстро
// Методы SHIFT/UNSHIFT выполняются медленно

// ПЕРЕБОР ЭЛЕМЕНТОВ
//Вариант через цикл for
let arr1 = ['Apple', 'Orange', 'Pear'];
for (let i = 0; i < arr1.length; i++){
    console.log(arr1[i]);
} 

//Вариант через цикл for..of
let arr2 = ['Apple', 'Orange', 'Pear'];
for (let fruit of arr2) {
    console.log(fruit);
}

//ПЛОХОЙ ВАРИАНТ!!! for... in
let arr3 = ['Apple', 'Orange', 'Pear'];
for (let key in arr3) {
    console.log(arr3[key]);
}

//СВОЙСТВО Length
let fruits11 = [];
fruits11[122] = 'apple';
console.log(fruits11.length); //123

//length можно перезаписать
let arr31 = [1, 2, 3, 4, 5, 6];
console.log(arr31);
arr3.length = 3; //Укорачиваем длину массива
console.log(arr31); //1, 2, 3
arr3.length = 6; 
console.log(arr31);// 1, 2, 3, <3 empty items> массив не восстановлен

//new Array() Применяется редко 

let arr22 = new Array('Apple', 'Pear');

// new Array() Создает массив без элементов
let arr21 = new Array(2);
console.log(arr21[0]); //undefined
console.log(arr21.length);//2

//=============

// МНОГОМЕРНЫЕ МАССИВЫ
let matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
];
console.log(matrix);
console.log(matrix[1][1]); // 5

//ЗАДАЧА 1
//Скопирован ли массив?
//Что выведет следующий код?
let fruits01 = ['apple', 'pear', 'orange'];
let shoppingCart = fruits01;
shoppingCart.push('Banana');
console.log(fruits01.length); // 4 
//Потому что массивы - это объекты.
//Обе переменные ссылаются на один и тот же массив.

//ЗАДАЧА 2
//ОПЕРАЦИИ С МАССИВАМИ
/**
  Давайте произведём 5 операций с массивом.
Создайте массив styles с элементами «Джаз» и «Блюз».
Добавьте «Рок-н-ролл» в конец.
Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.
Удалите первый элемент массива и покажите его.
Вставьте «Рэп» и «Регги» в начало массива.
 */
//создайте массив
let styles = ['Jazz', 'Blues'];
console.log(styles);
//Добавьте «Рок-н-ролл» в конец.
styles.push(`Rock'n'Roll`);
console.log(styles);
//Замените значение в середине на «Классика». 
styles[Math.floor((styles.length - 1) / 2)] = 'Classic';
console.log(styles);
//Удалите первый элемент массива и покажите его.
console.log(styles.shift());
//Вставьте «Рэп» и «Регги» в начало массива.
styles.unshift('Rap', 'Raggey');
console.log(styles);

//ЗАДАЧА 3
//Вызов в контексте массива
//Какой будет результат? Почему?
let arr33 = ['a', 'b'];
arr33.push(function() {
    console.log(this);
});
arr33[2](); //[ 'a', 'b', [Function (anonymous)] ]
//arr[2]() - это синтаксически obj[method]()

// ЗАДАЧА 4
/* Сумма введённых чисел 
Напишите функцию sumInput(), которая:
Просит пользователя ввести значения, используя prompt и сохраняет их в массив.
Заканчивает запрашивать значения, когда пользователь введёт не числовое значение, пустую строку или нажмёт «Отмена».
Подсчитывает и возвращает сумму элементов массива.
P.S. Ноль 0 – считается числом, не останавливайте ввод значений при вводе «0».
 */
function sumInput() {
    let arr41 = [];
    let value = 0;
    while (true) {
        value = prompt('Введите число: ', 3);
        console.log({ value });
        if (!value || !isFinite(value)) break;
        arr41.push(+value);
    }
    let sum = 0;
    for (let number of arr41){
        sum += number;
    }
    return sum;
}
console.log(sumInput());

//ЗАДАЧА 5
//ПОДМАССИВ НАИБОЛЬШЕЙ СУММЫ
/*
На входе массив чисел, например: arr = [1, -2, 3, 4, -9, 6].
Задача: найти непрерывный подмассив в arr, сумма элементов в котором максимальна.
Функция getMaxSubSum(arr) должна возвращать эту сумму.
*/
function getMaxSubSum(arr09) {
    let maxSum = 0;

    for (let i = 0; i < arr09.length; i++) {
        let sumStart = 0;
        outer: for (let j = 0; j < arr09.length; j++) {
            if (arr09[j] < 0) {
                sumStart = 0;
                continue outer;
            }
            sumStart += arr09[j];
            maxSum = Math.max(maxSum, sumStart)
        }
    }
    return maxSum;
}
console.log(getMaxSubSum([-1, 3, 5, -9, 8]));
