// КОНСПЕКТ

// Создание объекта требует два шага:
// 1. Создаем функцию, которая задает тип объекта;
// 2. Создаем экземпляр объекта, используя new;

// Повторяем еще раз тему про this

// создаем тип объекта car

function car(mark, model, year) {
    this.mark = mark; // добавляем свойство марка
    this.model = model; // добавляем свойство модель
    this.year = year; // добавляем свойство год.
}

let myCar = new car('Toyota', 'Tundra', 2020);
console.log({myCar}); // myCar: Car {mark: "Toyota", model: "Tundra", year: 2020}
console.log(myCar); // Car {mark: "Toyota", model: "Tundra", year: 2020}

let momCar = new car('Toyota', 'Prius', 2021);
console.log({momCar}); // myCar: Car {mark: "Toyota", model: "Tundra", year: 2020}
console.log(momCar); // Car {mark: "Toyota", model: "Tundra", year: 2020}

function Person(name, age, sex) {
    this.name = name;
    this.age = age;
    this.sex = sex;
}

let lyda = new Person('Lyda', 10, 'F');
console.log({lyda});
console.log(lyda);

let anna = new Person('Anna', 35, 'F');

function car(mark, model, year, owner) {
    this.mark = mark;
    this.model = model;
    this.year = year;
    this.owner = owner;
}

let firstCar = new car('Toyota', 'Ist', 2020, lyda);
console.log({firstCar}); // car {mark: "Toyota", model: "Ist", year: 2020, owner: Person}
console.log(firstCar); // car {mark: "Toyota", model: "Ist", year: 2020, owner: Person}

let secondCar = new car('Nissan', 'Teana', 2021, anna);

console.log({secondCar}); // car {mark: "Nissan", model: "Teana", year: 2021, owner: Person}
console.log(secondCar); // car {mark: "Nissan", model: "Teana", year: 2021, owner: Person}

console.log(secondCar.owner.name); // Anna




// ПРОЗРАЧНОЕ КЭШИРОВАНИЕ

function slow(x) {
    console.log(`Called with ${x}`);
    return x;
}

function cachingDecorator(func) {
    let cache = new Map();

    return function(x) {
        if (cache.has(x)) {
            return cache.get(x);
        }

        let result = func(x);

        cache.set(x, result);
        return result;
    };
}

slow = cachingDecorator(slow);

console.log(slow(1));
console.log(`Again: ${slow(1)}`);
console.log(slow(2));
console.log(`Again: ${slow(2)}`);


// Повторяем рекурсию

function f(n) {
    console.log(n); // 5, 4, 3, 2, 1, 0
    return n
        ? n * f(n-1)
        : 1; // 5 * 4 * 3 * 2 * 1
}
console.log(f(5)); // 120


