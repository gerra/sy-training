// ЗАДАЧА EVAL
'use strict';
// Eval-калькулятор
// важность: 4
// Создайте калькулятор, который запрашивает ввод какого-нибудь арифметического выражения и возвращает результат его вычисления.

// В этой задаче нет необходимости проверять полученное выражение на корректность, просто вычислить и вернуть результат.

// РЕШЕНИЕ ЗАДАЧИ: 
console.log('ЗАДАЧА eval')

let calculator = prompt('Введите арифметическое выражение:', '2*2+6');
console.log(eval(calculator)); // 10

// ЗАДАЧА INTL

// Отсортируйте массив с буквой ё
// важность: 5
// Используя Intl.Collator, отсортируйте массив:

// let animals = ['тигр', 'ёж', 'енот', 'ехидна', 'АИСТ', 'ЯК'];

// // ... ваш код ...

// console.log( animals ); // АИСТ,ёж,енот,ехидна,тигр,ЯК
// В этом примере порядок сортировки не должен зависеть от регистра.

// Что касается буквы 'ё', то мы следуем обычным правилам сортировки буквы ё, по которым «е» и «ё» считаются одной и той же буквой, за исключением случая, когда два слова отличаются только в позиции буквы «е» / «ё» – тогда слово с «е» ставится первым.


// РЕШЕНИЕ ЗАДАЧИ INTL
console.log('РЕШЕНИЕ ЗАДАЧИ INTL');

let animals = ['тигр', 'ёж', 'енот', 'ехидна', 'АИСТ', 'ЯК'];

let collator = new Intl.Collator();

animals.sort((a, b) => collator.compare(a, b));

console.log(animals); // [ 'АИСТ', 'ёж', 'енот', 'ехидна', 'тигр', 'ЯК' ]

