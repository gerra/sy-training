// КОНСПЕКТ

// РЕШЕНИЕ АСИНХРОННОСТИ ЧЕРЕЗ КОЛБЭКИ:

// console.log('Request data...');

// // это первый колбэк
// setTimeout(() => {
//     const backEndData = {
//         server: 'aws',
//         port: 2000,
//         status: 'working'
//     }

//     console.log('Preparing data...', backEndData);
// // это второй колбэк
//     setTimeout(() => {
//         backEndData.modified = true;
        
//         console.log('Preparing data...', backEndData);
// // это третий колбэк
//         setTimeout(() => {
//             backEndData.userName = 'Abel';

//             console.log('Preparing data...', backEndData);
// // это четвертый колбэк
//             setTimeout(() => {
//                 backEndData.admin = 'Noah';

//                 console.log('Preparing data...', backEndData);
// // это пятый колбэк
//                 setTimeout(() => {
//                     backEndData.password = '1234567890';

//                     console.log('Data received!', backEndData);
// // это шестой колбэк
//                     setTimeout(() => {
//                         backEndData.update = true;

//                         console.log('Data update.', backEndData)
//                     }, 2000)
//                 }, 2000)
//             }, 2000)
//         }, 2000)
//     }, 2000)
// }, 2000)




// РЕШЕНИЕ АСИНХРОННОСТИ ЧЕРЕЗ ПРОМИСЫ:

// console.log('Request data...');

// const p = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         const backEndData = {
//             server: 'aws',
//             port: 2000,
//             status: 'working',
//     }

//         console.log('Preparing data 1...', backEndData);

//         resolve(backEndData); // это обязательное поле!!!
//             // передаем backEndData в resolve 
//     }, 2000)
// })

// p
//     .then(data => {
//         return new Promise((resolve, reject) => {
//             setTimeout(() => {

//                 data.modified = true;

//                 reject(data);
//             }, 2000)
//         });
//     })
//     .then(modifiedData => {
//         console.log('Preparing data 2...', modifiedData);
//         return modifiedData;
//         // resolve(modifiedData);
//     })
//     .then(userNameData => {
//         return new Promise((resolve, reject) => {
//             setTimeout(() => {

//                 userNameData.userName = 'Abel';

//                 console.log('Preparing data 3...', userNameData);

//                 resolve(userNameData);
//             }, 2000)
//         })
//     })
//     .then(adminData => {
//         return new Promise((resolve, reject) => {
//             setTimeout(() => {
//                 adminData.admin = 'Noah';

//                 console.log('Preparing data 4...', adminData);

//                 resolve(adminData);
//             }, 2000)
//         })
//     })
//     .then(passwordData => {
//         return new Promise((resolve, reject) => {
//             setTimeout(() => {
//                 passwordData.password = '1234567890';

//                 resolve(passwordData);
//             }, 2000)
//         })
//     })
//     .then(data => {
//         console.log('Preparing data 5...', data);
//         return data;
//     })
//     .then(updateData => {
//         return new Promise((resolve, reject) => {
//             setTimeout(() => {
//                 updateData.update = true;

//                 resolve(updateData);
//             }, 2000)
//         })
//     })
//     .then(data => {
//         console.log('Data received!', data);
//         return data;
//     })
//     .catch(
//         err => console.error('Error:', err)
//     )
//     .finally(() => console.log('Done!'));


// СЛЕДУЮЩАЯ ФУНКЦИЯ ПРОМИСОВ

const sleep = ms => {
    return new Promise(resolve => {
        setTimeout(() => resolve(), ms)
    })
}

sleep(2000).then(() => console.log('After 2 sec.'));
sleep(3000).then(() => console.log('After 3 sec.'));
sleep(4000).then(() => console.log('After 4 sec.'));
sleep(5000).then(() => console.log('After 5 sec.'));


