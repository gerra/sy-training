// ЗАДАЧА 1

// Деструктурирующее присваивание
// важность: 5
// У нас есть объект:

// let user = {
//   name: "John",
//   years: 30
// };
// Напишите деструктурирующее присваивание, которое:

// свойство name присвоит в переменную name.
// свойство years присвоит в переменную age.
// свойство isAdmin присвоит в переменную isAdmin (false, если нет такого свойства)
// Пример переменных после вашего присваивания:

// let user = { name: "John", years: 30 };

// ваш код должен быть с левой стороны:
// ... = user

// alert( name ); // John
// alert( age ); // 30
// alert( isAdmin ); // false

// РЕШЕНИЕ ЗАДАЧИ 1

console.log('ЗАДАЧА 1');

let user = {
  name: "John",
  years: 30
};

let {
    name,
    years: age,
    isAdmin = false,
} = user;

console.log(name); // John
console.log(age); // 30
console.log(isAdmin); // false


// ЗАДАЧА 2

// Максимальная зарплата
// важность: 5
// У нас есть объект salaries с зарплатами:

// let salaries = {
//   "John": 100,
//   "Pete": 300,
//   "Mary": 250
// };
// Создайте функцию topSalary(salaries), которая возвращает имя самого высокооплачиваемого сотрудника.

// Если объект salaries пустой, то нужно вернуть null.
// Если несколько высокооплачиваемых сотрудников, можно вернуть любого из них.
// P.S. Используйте Object.entries и деструктурирование, чтобы перебрать пары ключ/значение.


// РЕШЕНИЕ ЗАДАЧИ 2

console.log('ЗАДАЧА 2');

let salaries = {
  "John": 100,
  "Pete": 300,
  "Mary": 250,
  "Matt": 1000,
  "Mark": 1500,
  "Luke": 500,
};

function topSalary(salaries) {
    let name = null; // создаем переменную name и даем ей значение null, если в объекте нет никаких значений.
    let maxPrice = 0; // создаем переменную maxPrice, которую будем сравнивать со всеми значениями объекта.
    for (let [key, value] of Object.entries(salaries)) {
        if (maxPrice < value) {
            maxPrice = value; // Если maxPrice меньше value, то мы присваиваем ему значение value, то есть он будет использоваться в дальнейших сравнениях в цикле.
            name = key; // здесь мы присваиваем значение key для name
        };
    }
    return name;
}

console.log(topSalary(salaries)); // Mark