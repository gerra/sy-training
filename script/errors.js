// ЗАДАЧА 1

// Наследование от SyntaxError
// важность: 5
// Создайте класс FormatError, который наследует от встроенного класса SyntaxError.

// Класс должен поддерживать свойства message, name и stack.

// Пример использования:
// let err = new FormatError("ошибка форматирования");

// alert( err.message ); // ошибка форматирования
// alert( err.name ); // FormatError
// alert( err.stack ); // stack

// alert( err instanceof FormatError ); // true
// alert( err instanceof SyntaxError ); // true 

// РЕШЕНИЕ ЗАДАЧИ 1
console.log('ЗАДАЧА 1');

class FormatError extends SyntaxError { // наследуем класс из SyntaxError
    constructor(message) { 
        super(message); // создаем метод super в который передаем message
        this.name = 'FormatError'; // присваиваем ключу name значение 'FormatError'
    }
}

let err = new FormatError('ошибка форматирования');

console.log(err.message); // ошибка форматирования
console.log(err.name); // FormatError
console.log(err.stack); // FormatError: ошибка форматирования

console.log(err instanceof FormatError); // true
console.log(err instanceof SyntaxError); // true 
