// ЗАДАЧА 1

// Создайте дату
// важность: 5
// Создайте объект Date для даты: 20 февраля 2012 года, 3 часа 12 минут. Временная зона – местная.

// РЕШЕНИЕ ЗАДАЧИ 1

console.log('ЗАДАЧА 1');

let feb20_2012 = new Date(2012, 1, 20, 3, 12);
console.log(feb20_2012); //Mon Feb 20 2012 03:12:00 GMT+1100 (Владивосток, стандартное время)

// ЗАДАЧА 2

// Покажите день недели
// важность: 5
// Напишите функцию getWeekDay(date), показывающую день недели в коротком формате: «ПН», «ВТ», «СР», «ЧТ», «ПТ», «СБ», «ВС».
// Например:
// let date = new Date(2012, 0, 3);  // 3 января 2012 года
// alert( getWeekDay(date) );        // нужно вывести "ВТ"

// РЕШЕНИЕ ЗАДАЧИ 2

console.log('ЗАДАЧА 2');

let date = new Date(2012, 0, 3);  // 3 января 2012 года
console.log(date); 
function getWeekDate(date) {
    let day = date.getDay();
    let weekDay;
    switch(day) {              //делаем переключение в зависимости от номера дня недели
        case 0: weekDay = 'ВС';
        break;
        case 1: weekDay = 'ПН';
        break;
        case 2: weekDay = 'ВТ';
        break;
        case 3: weekDay = 'СР';
        break;
        case 4: weekDay = 'ЧТ';
        break;
        case 5: weekDay = 'ПТ';
        break;
        case 6: weekDay = 'СБ';
        break;
    }
    return weekDay;
}
getWeekDate(date);
console.log(getWeekDate(date)); // ВТ

//ЗАДАЧА 3
// День недели в европейской нумерации
// важность: 5
// В Европейских странах неделя начинается с понедельника (день номер 1), затем идёт вторник (номер 2) и так до воскресенья (номер 7). Напишите функцию getLocalDay(date), которая возвращает «европейский» день недели для даты date.
// let date = new Date(2012, 0, 3);  // 3 января 2012 года
// alert( getLocalDay(date) );       // вторник, нужно показать 2

// РЕШЕНИЕ ЗАДАЧИ 3
console.log('ЗАДАЧА 3');

let thirdDate = new Date(2012, 0, 8); // Создаем новую дату

function getLocalDay(thirdDate) {
    let thirdDay = thirdDate.getDay(); // возвращаем номер дня недели.
// 0
    if (thirdDay == 0) { // если день недели вс (равен 0), то переназначаем его на 7
        thirdDay = 7;
    }
// 7
    return thirdDay;
}
console.log(getLocalDay(thirdDate)); // 7


// ЗАДАЧА 4
// Какой день месяца был много дней назад?
// важность: 4
// Создайте функцию getDateAgo(date, days), возвращающую число, которое было days дней назад от даты date.

// К примеру, если сегодня двадцатое число, то getDateAgo(new Date(), 1) вернёт девятнадцатое и getDateAgo(new Date(), 2) – восемнадцатое.

// Функция должна надёжно работать при значении days=365 и больших значениях:

// let date = new Date(2015, 0, 2);

// alert( getDateAgo(date, 1) ); // 1, (1 Jan 2015)
// alert( getDateAgo(date, 2) ); // 31, (31 Dec 2014)
// alert( getDateAgo(date, 365) ); // 2, (2 Jan 2014)
// P.S. Функция не должна изменять переданный ей объект date.

//РЕШЕНИЕ ЗАДАЧИ 4
console.log('ЗАДАЧА 4');

let dateAgo = new Date(2021, 7, 10); //Tue Aug 10 2021

function getDateAgo(dateAgo, a) { // принимаем дату и количество дней
    let dateCurrent = new Date(dateAgo); // создаем дублирующую переменную, чтобы date1 не изменялась
    dateCurrent.setDate(dateAgo.getDate() - a); // устонавливаем date2 
    return dateCurrent;
}
console.log(getDateAgo(dateAgo, 10)); // Sat Jul 31 2021
console.log(getDateAgo(dateAgo, 365));// Mon Aug 10 2020
console.log(getDateAgo(dateAgo, 30)); // Sun Jul 11 2021


// ЗАДАЧА 5

// Последнее число месяца?
// важность: 5
// Напишите функцию getLastDayOfMonth(year, month), возвращающую последнее число месяца. Иногда это 30, 31 или даже февральские 28/29.
// Параметры:
// year – год из четырёх цифр, например, 2012.
// month – месяц от 0 до 11.
// К примеру, getLastDayOfMonth(2012, 1) = 29 (високосный год, февраль).

//РЕШЕНИЕ ЗАДАЧИ 5

console.log('ЗАДАЧА 5');

function getLastDayOfMonth(year5, month5) { // принимаем год и месяц
    let lastDay = new Date(year5, month5 + 1, 0); // добавляем к месяцу +1, а при дне равном 0, он автоматически вычитает один день. Получается переход на последнюю дату предыдущего месяца.
    return lastDay.getDate();
}

console.log(getLastDayOfMonth(2021, 0)); // 31
console.log(getLastDayOfMonth(2021, 1)); // 28
console.log(getLastDayOfMonth(2021, 2)); // 31
console.log(getLastDayOfMonth(2021, 3)); // 30


// ЗАДАЧА 6

// Сколько сегодня прошло секунд?
// важность: 5
// Напишите функцию getSecondsToday(), возвращающую количество секунд с начала сегодняшнего дня.
// Например, если сейчас 10:00, и не было перехода на зимнее/летнее время, то:
// getSecondsToday() == 36000 // (3600 * 10)
// Функция должна работать в любой день, т.е. в ней не должно быть конкретного значения сегодняшней даты.

// РЕШЕНИЕ ЗАДАЧИ 6

console.log('ЗАДАЧА 6');

function getSecondsToday() {
    let timeCurrent = new Date();
    let hoursCurrent = timeCurrent.getHours() * 3600;
    let minutesCurrent = timeCurrent.getMinutes() * 60;
    let secondsCurrent = timeCurrent.getSeconds();
    let sec = hoursCurrent + minutesCurrent + secondsCurrent;
    console.log(sec);
}

getSecondsToday();

// ЗАДАЧА 7

// Сколько секунд осталось до завтра?
// важность: 5
// Создайте функцию getSecondsToTomorrow(), возвращающую количество секунд до завтрашней даты.

// Например, если сейчас 23:00, то:

// getSecondsToTomorrow() == 3600
// P.S. Функция должна работать в любой день, т.е. в ней не должно быть конкретного значения сегодняшней даты.

// РЕШЕНИЕ ЗАДАЧИ 7

console.log('ЗАДАЧА 7');

function getSecondsToTomorrow() {
    let timeNow = new Date(); // создаем переменную с текущим временем.
    let hoursNow = timeNow.getHours() * 3600;
    let minutesNow = timeNow.getMinutes() * 60;
    let secondsNow = timeNow.getSeconds();
    let secondsInDay = 24 * 3600;

    let secondsToTomorrow = secondsInDay - hoursNow - minutesNow - secondsNow; // Вычитаем из общего количества секунд в сутках общее колисество секунд текущего времени.
    console.log(secondsToTomorrow); // выводим в консоль
}

getSecondsToTomorrow();


// ЗАДАЧА 8

// Форматирование относительной даты
// важность: 4
// Напишите функцию formatDate(date), форматирующую date по следующему принципу:

// Если спустя date прошло менее 1 секунды, вывести "прямо сейчас".
// В противном случае, если с date прошло меньше 1 минуты, вывести "n сек. назад".
// В противном случае, если меньше часа, вывести "m мин. назад".
// В противном случае, полная дата в формате "DD.MM.YY HH:mm". А именно: "день.месяц.год часы:минуты", всё в виде двух цифр, т.е. 31.12.16 10:00.
// Например:
// alert( formatDate(new Date(new Date - 1)) ); // "прямо сейчас"
// alert( formatDate(new Date(new Date - 30 * 1000)) ); // "30 сек. назад"
// alert( formatDate(new Date(new Date - 5 * 60 * 1000)) ); // "5 мин. назад"
// // вчерашняя дата вроде 31.12.2016, 20:00
// alert( formatDate(new Date(new Date - 86400 * 1000)) );

// РЕШЕНИЕ ЗАДАЧИ 8
console.log('ЗАДАЧА 8');

let datePoint = new Date(2021, 7, 7, 15, 23);

function formatDate(datePoint) {
    let dateNow = new Date(); 
    let yearPoint = datePoint.getFullYear();  // выводим значение года
    let monthPoint = datePoint.getMonth() + 1;// выводим значение месяца (т.к. месяцы начинаются с 0, то нужно прибавить +1)
    let dayPoint = datePoint.getDate();       // выводим значение дня
    let hoursPoint = datePoint.getHours();    // выводим значение часов
    let minutesPoint = datePoint.getMinutes();// выводим значение минут
    let oneMinute = 60;
    let oneHour = 3600;
    let millisecond = 1000;

    // Добавляем нули к числам, если они меньше 10.
    monthPoint = monthPoint < 10 ? `0${monthPoint}` : monthPoint;
    dayPoint = dayPoint < 10 ? `0${dayPoint}` : dayPoint;
    hoursPoint = hoursPoint < 10 ? `0${hoursPoint}` : hoursPoint;[]
    minutesPoint = minutesPoint < 10 ? `0${minutesPoint}` : minutesPoint;

    if (datePoint > dateNow) {
        console.log('Введена неверная дата.');
    } else {
        let dateResult = (dateNow - datePoint) / millisecond; // переводим милисекунды в секунды.
        if (dateResult < 1) {
            return 'прямо сейчас';
        } else if (dateResult > 1 && dateResult < oneMinute) {
            return `${dateResult} секунд назад`;
        } else if (dateResult > oneMinute && dateResult < oneHour) {
            return `${Math.floor(dateResult/oneMinute)} минут назад`;
        } else {
            return `${dayPoint}:${monthPoint}:${yearPoint}, ${hoursPoint}:${minutesPoint}`;
        }
    }
}

console.log(formatDate(datePoint));
