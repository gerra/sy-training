//ТИПЫ ДАННЫХ. ЧИСЛА

console.log(23e9);
console.log(1e-9);
let num = 255;
console.log(num.toString(16));

//ОКРУГЛЕНИЕ ДО ЦЕЛОГО ЧИСЛА
console.log(Math.floor(4.4)); // Округление в меньшую сторону
console.log(Math.ceil(4.4)); // Округление в большую сторону
console.log(Math.round(4.4)); // Округление до ближайшего целого
console.log(Math.trunc(4.4)); // Удаляет дробь без округления

//ОКРУГЛЕНИЕ ДО ДРОБНОЙ ЧАСТИ
// Способ первый: умножить и разделить
let num0 = 1.23456;
console.log(Math.floor(num0 * 100) / 100);

//Способ второй: toFixed(n);
let num1 = 12.34;
console.log(num1.toFixed(1)); //12.3

let num2 = 12.36;
console.log(num2.toFixed(1)); // 12.4

console.log(1e99990); //Infinity

console.log(0.1 + 0.2); // 0.30000000000000004

console.log(0.1.toFixed(20)); // 0.10000000000000000555

console.log(999999999999999999); //1000000000000000000

console.log(NaN === NaN); //false

//isNaN() Преобразует значение в число и проверяет является ли оно NaN
console.log(isNaN(NaN)); //true потому что NaN
console.log(isNaN('str')); //true потому что это не число
console.log(isNaN(40)); //false потому что это число.

//isFinite() Преобразует аргумент в число и возвращает true если оно не равно NaN/Infinity/-Infinity
console.log(isFinite('15')); //true
console.log(isFinite('ddd')); //false
console.log(isFinite(Infinity)); //false
console.log(isFinite('')); // true потому что пустая строка = 0

//isFinite проверяет, есть ли в строке число:
let num3 = +prompt('Enter a number:', '');
isFinite(num3) == false
    ? console.log('Incorrect value!')
    : console.log('Well done!');

//Сравнение Object.is
console.log(Object.is(0, 0)); //true
console.log(Object.is(NaN, NaN)); //true

//parseInt & parseFloat
console.log(parseInt('100px')); //100
console.log(parseFloat('12.4em')); //12.4

console.log(parseInt('12.8')); //12 - !!! число не округляется.
console.log(parseFloat('12.3.4.4')); //12.3

console.log(parseInt('s111')); //NaN - буква на первом символе

//parseInt(str, radix) может читать строки с 1,2,3, ...ричными числами
console.log(parseInt('0xff', 16)); //255
console.log(parseInt('ff', 16)); //255

//ДРУГИЕ МАТЕМАТИЧЕСКИЕ ФУНКЦИИ
//Math.random() возвращает число от 0 до 1
console.log(Math.random() ); //0.13954509083564304
console.log(Math.random() * 1e10); //271811792.88798374

//Math.max(a, b, c ...) / Math.min(a, b, c....)
console.log(Math.max(1, 3, 4, -2, 66, 3e3)); //3000
console.log(Math.min(1, 3, 4, -2, 66, 3e3)); //-2

//Math.pow(n, power) Возведение в степень
console.log(Math.pow(2, 8)); //256

//ЗАДАЧА1

/**
Сумма пользовательских чисел
важность: 5
Создайте скрипт, который запрашивает ввод двух чисел (используйте prompt) и после показывает их сумму.
P.S. Есть «подводный камень» при работе с типами.
 */

let num5 = prompt('enter a', '10');
let num6 = prompt('enter b', '10');
console.log(num5 + num6);

//ЗАДАЧА 2
/*
Почему 6.35.toFixed(1) == 6.3?
важность: 4
Методы Math.round и toFixed, согласно документации, округляют до ближайшего целого числа: 0..4 округляется в меньшую сторону, тогда как 5..9 в большую сторону.
Например:
alert( 1.35.toFixed(1) ); // 1.4
Но почему в примере ниже 6.35 округляется до 6.3?
alert( 6.35.toFixed(1) ); // 6.3
Как правильно округлить 6.35?
*/

console.log(1.35.toFixed(1));
console.log(6.35.toFixed(1)); 
console.log(Math.round(6.35 * 10) / 10);

//ЗАДАЧА 3
/*
Ввод числового значения
важность: 5
Создайте функцию readNumber, которая будет запрашивать ввод числового значения до тех пор, пока посетитель его не введёт.
Функция должна возвращать числовое значение.
Также надо разрешить пользователю остановить процесс ввода, отправив пустую строку или нажав «Отмена». В этом случае функция должна вернуть null.
*/

//Это первый вариант решения
function readNumber() {
    let num6 = prompt('Введите число:', 5);
    num6 === null
        ? console.log('null')
        : isNaN(num6) === true
            ? num6 = prompt('Введите число:', 5)
            : console.log('Поздравляем! вы сделали верно!');
}

readNumber();

//Это второй вариант решения
function readNumber1() {
    let num7; 
    do {
         num7 = prompt('Введите число:', 7);
    } while (!isFinite(num7));
    if (num7 === null || num7 === '') return null;

    return +num7;
}

console.log(`Поздравляем! вы ввели верное число: ${readNumber1()}`);

//ЗАДАЧА 4
/*
Бесконечный цикл по ошибке
важность: 4
Этот цикл – бесконечный. Он никогда не завершится, почему?
*/
let counter = 0;
while (counter != 10){
    counter += 0.2;
} // происходит потеря точности при работе с дробями

//ЗАДАЧА 5
/*
Случайное число от min до max
важность: 2
Встроенный метод Math.random() возвращает случайное число от 0 (включительно) до 1 (но не включая 1)

Напишите функцию random(min, max), которая генерирует случайное число с плавающей точкой от min до max (но не включая max).
*/

// первое решение
function random(min, max) {
    let ran;
    if (min < max) {
        do {
            ran = (Math.random() * 10)
        }   while (ran < min || ran > max) 
    } else {
        let x = min;
        min = max;
        max = x;
        do {
            ran = (Math.random() * 10)
        }   while (ran < min || ran > max) 
    }
    return ran;
}
console.log(random(44, 4));
console.log(random(44, 4));
console.log(random(44, 4));
console.log(random(44, 4));
console.log(random(3,16));
console.log(random(3,16));
console.log(random(3,16));

// Второе решение
function random1(a, b) {
    let min = a < b ? a : b;
    let max = a > b ? a : b;
    return min + Math.random() * (max - min)
}

console.log(random1(15,33));
console.log(random1(44, 2));
console.log(random1(44, 2));
console.log(random1(44, 2));
console.log(random1(44, 2));
console.log(random1(15,33));
console.log(random1(15,33));
console.log(random1(15,33));

//ЗАДАЧА 6

/*
Случайное целое число от min до max
важность: 2
Напишите функцию randomInteger(min, max), которая генерирует случайное целое (integer) число от min до max (включительно).
Любое число из интервала min..max должно появляться с одинаковой вероятностью.
*/

function randomInteger(a, b) {
    let min = a < b ? a : b;
    let max = a > b ? a : b;
    return parseInt(min + Math.random() * (max + 1 - min));
}

console.log(randomInteger(22, 3));
console.log(randomInteger(22, 3));
console.log(randomInteger(22, 3));
console.log(randomInteger(1,3));
console.log(randomInteger(1,3));
console.log(randomInteger(1,3));
console.log(randomInteger(1,3));
console.log(randomInteger(1,3));

