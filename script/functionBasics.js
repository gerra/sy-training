// ПРИМЕР ИЗ УРОКА
console.log('ПРИМЕР 1. Про функции');
let userName = 'Джон';
function showMessage() {
    let message = `Здравствуйте, ${userName}!` ;
    console.log(message);
}
showMessage();

// ПРИМЕР ИЗ УРОКА.
console.log('ПРИМЕР 2. Про параметры функции');
function message(from, text) {
    console.log(`${from}: ${text}`);
}
message('Джон', 'привет!');
message('John', 'how are you?');



//ЗАДАЧА 1
/* 
Обязателен ли "else"?
важность: 4
Следующая функция возвращает true, если параметр age больше 18.
*/

// РЕШЕНИЕ ЗАДАЧИ 1.1
console.log('ЗАДАЧА 1.1');
let age = 19;
function checkAge(age){
    if (age > 18) {
        return true;
    } else {
        return confirm('Родители разрешили?');
    }
}
if (checkAge(age)) {
    console.log('Доступ получен.');
} else {
    console.log('Доступ закрыт');
}

// РЕШЕНИЕ ЗАДАЧИ 1.2
console.log('ЗАДАЧА 1.2');
age = 15;
function checkAge(age) {
    if (age > 18) {
        return true;
    }
    return confirm('Родители разрешили?');
}
if (checkAge(age)) {
    console.log('Доступ получен.');
} 
    console.log('Доступ закрыт.');


// ЗАДАЧА 2
/*
Перепишите функцию, используя оператор '?' или '||'
Следующая функция возвращает true, если параметр age больше 18.
В ином случае она задаёт вопрос confirm и возвращает его результат.

function checkAge(age) {
  if (age > 18) {
    return true;
  } else {
    return confirm('Родители разрешили?');
  }
}
*/

// РЕШЕНИЕ ЗАДАЧИ 2.1
console.log('РЕШЕНИЕ ЗАДАЧИ 2.1');
function checkAge(age) {
    return (age > 18) 
        ? true 
        : confirm('Родители разрешили?');
}

// РЕШЕНИЕ ЗАДАЧИ 2.2
console.log('ЗАДАЧА 2.2');
function checkAge(age) {
    return (age > 18) || confirm('Родители разрешили?');
}

//ЗАДАЧА 3
/*
Функция min(a, b)
Напишите функцию min(a,b), которая возвращает меньшее из чисел a и b.
Пример вызовов:
min(2, 5) == 2
min(3, -1) == -1
min(1, 1) == 1
*/

// РЕШЕНИЕ ЗАДАЧИ 3.
console.log('ЗАДАЧА 3');
function min(a,b) {
    return (a < b)
        ? a
        : b;
}
console.log(min(4,8));

//ЗАДАЧА 4
/*
Функция pow(x,n)
апишите функцию pow(x,n), которая возвращает x в степени n. Иначе говоря, умножает x на себя n раз и возвращает результат.
pow(3, 2) = 3 * 3 = 9
pow(3, 3) = 3 * 3 * 3 = 27
pow(1, 100) = 1 * 1 * ...* 1 = 1
Создайте страницу, которая запрашивает x и n, а затем выводит результат pow(x,n).
*/

// РЕШЕНИЕ ЗАДАЧИ 4

console.log('ЗАДАЧА 4');
function pow(x,n) {
    if (n < 1) {
        return false;
    }
    let exp = x;
    for (let i = 1; i < n; i++) {
    x = exp * x;
    }
    return x;
}
console.log(pow(6,5));
