// ЗАДАЧА 1

// Перепишите класс
// важность: 5
// Класс Clock написан в функциональном стиле. Перепишите его, используя современный синтаксис классов.

// P.S. Часики тикают в консоли. Откройте её, чтобы посмотреть.

// function Clock({ template }) {
//     let timer;
  
//     function render() {
//         let date = new Date();
  
//         let hours = date.getHours();
//         if (hours < 10) hours = '0' + hours;
  
//         let mins = date.getMinutes();
//         if (mins < 10) mins = '0' + mins;
  
//         let secs = date.getSeconds();
//         if (secs < 10) secs = '0' + secs;
  
//         let output = template
//             .replace('h', hours)
//             .replace('m', mins)
//             .replace('s', secs);
  
//         console.log(output);
//     }
  
//     this.stop = function() {
//         clearInterval(timer);
//     };
  
//     this.start = function() {
//         render();
//         timer = setInterval(render, 1000);
//     };
  
// }
  
// let clock = new Clock({template: 'h:m:s'});
// clock.start();

class newClock {
    constructor({ template }) { // тут главное правильно оформить constructor
        this.template = template;
    }

    // убрал this и function
    start() {
        this.render(); // поставил this
        this.timer = setInterval(() => this.render(), 1000); // поставил this. Иначе не работает.
    };

    // чтобы все работало нужно убрать this и function
    stop() {
        clearInterval(this.timer); // поставил this
    };

    // Здесь нужно убрать function 
    render() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = `0${hours}`;
  
        let mins = date.getMinutes();
        if (mins < 10) mins = `0${mins}`;

        let secs = date.getSeconds();
        if (secs < 10) secs = `0${secs}`;

        let output = this.template // без this работать не будет
            .replace('h', hours) // метод .replace заменяет 'h' на hours
            .replace('m', mins) // метод .replace заменяет 'm' на mins
            .replace('s', secs); // метод .replace заменяет 's' на secs

        console.log(output);
    }
}

let clock = new newClock({ template: 'h:m:s' });
clock.start();
