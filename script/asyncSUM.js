// КОНСПЕКТ 


// await работает с «thenable»–объектами

class Thenable {
    constructor(num) {
        this.num = num;
    }
    then(resolve, reject) {
        console.log(resolve);
        setTimeout(() => resolve(this.num * 2), 2000);
    }
}

async function f() {
    let result = await new Thenable(1);
    console.log(result);
}

f(); // 2 (через две секунды)


// Асинхронные методы классов

class Waiter {
    async wait() {
        return await Promise.resolve(1);
    }
}

new Waiter()
    .wait()
    .then(console.log) // 1

