// КОНСПЕКТ

// "use strict"

// let promise = new Promise((resolve) => {
//     let n = 2;
//     resolve(n);
// }).then((result) => {
//     console.log(result); // 2
//     return result *= result;
// }).then((result) => {
//     console.log(result); // 4
//     return result *= result;
// }).then((result) => {
//     console.log(result); // 16
//     return result *= result; 
// }).then((result) => {
//     console.log(result); // 256
//     return result *= result;
// }).then((result) => {
//     console.log(result); // 65536
// });


// let promise = Promise.resolve();

// promise.then(() => console.log(1)); // потом 1

// console.log(2); // сначала 2


Promise.resolve()
    .then(console.log(1))
    .then(console.log(2));
