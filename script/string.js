//СТРОКИ. STRING

//Длина строки
console.log(`My\n`.length);

//ДОСТУП К СИМВОЛАМ [ ] и charAt()
let str = `Hello!!`;
console.log(str[0]);
console.log(str[1]);
console.log(str[2]);
console.log(str[3]);
console.log(str[4]);
console.log(str[str.length - 1]);

console.log(str.charAt(0));
console.log(str.charAt(1));
console.log(str.charAt(2));
console.log(str.charAt(3));
console.log(str.charAt(4));
console.log(str.charAt(str.length - 1));

console.log(str[44]); //undefined
console.log(str.charAt(33)); //Пустая строка

//Изменение регистра

console.log('Interface'.toLowerCase()); // interface
console.log('Interface'.toUpperCase()); // INTERFACE

//Перевод в нижний регистр конкретный символ
console.log('Interface'[5].toUpperCase()); //F

//Поиск подстроки str.indexOf(substr, pos)
let str1 = 'Widget with id';
console.log(str1.indexOf('Widget'));//0
console.log(str1.indexOf('widget'));//-1
console.log(str1.indexOf('id'));//1
console.log(str1.indexOf('id', 2));//12

//ЗАПУСКАЕМ ЦИКЛ ДЛЯ ПОИСКА ВСЕХ ПОДСТРОК
let str2 = 'Так всякое дерево доброе приносит и плоды добрые, а худое дерево приносит и плоды худые\. Не может дерево доброе приносить плоды худые, ни дерево худое приносить плоды добрые\. Всякое дерево, не приносящее плода доброго, срубают и бросают в огонь\.(От Матфея 7:17-19)'
let target = 'плод'; // цель поиска.
let pos = 0;
while (true) {
    let foundPos = str2.indexOf(target, pos);
    if (foundPos === -1) break;
    console.log(`Найдено в этой позиции: ${foundPos}`);
    pos = foundPos + 1; // Продолжаем со следующей позиции.
}

//ТОТ ЖЕ АЛГОРИТМ, ТОЛЬКО КОРОЧЕ
let str3 = 'Так всякое дерево доброе приносит и плоды добрые, а худое дерево приносит и плоды худые\. Не может дерево доброе приносить плоды худые, ни дерево худое приносить плоды добрые\. Всякое дерево, не приносящее плода доброго, срубают и бросают в огонь\.(От Матфея 7:17-19)'
let target = 'дерево';
let pos3 = -1;
while ((pos3 = str3.indexOf(target, pos3 + 1)) != -1) {
    console.log(`Найдено в этой позиции: ${pos3}`);
}

//str.lastIndexOf(substr, position) ищет с конца строки.
let str4 = 'Widget with id';
if (str4.indexOf('Widget') != -1) { // Дело в том, что возвращается 0, поэтому if считывает это как false
    console.log('Совпадение есть!');
}

//Другой вариант
let str5 = 'Widget with id';
if (~str5.indexOf('Widget')) { // ~ побитовый оператор НЕ. = ЕСЛИ НЕ НАЙДЕНА
    console.log('Совпадение есть!');
}

// if (~str.indexOf(…)) означает «если найдено».

//includes, startsWith, endsWith
console.log('Так всякое дерево доброе приносит и плоды добрые, а худое дерево приносит и плоды худые'.includes('дерево')); //true
console.log('Так всякое дерево доброе приносит и плоды добрые, а худое дерево приносит и плоды худые'.includes('дом')); //false
console.log('Так всякое дерево доброе приносит и плоды добрые, а худое дерево приносит и плоды худые'.startsWith('Так'));//true
console.log('Так всякое дерево доброе приносит и плоды добрые, а худое дерево приносит и плоды худые'.endsWith('худые'));//true

//Получение подстроки SUBSTRING, SUBSTR, SLICE

let str6 = 'Так всякое дерево доброе приносит и плоды добрые, а худое дерево приносит и плоды худые';
console.log(str6.slice(0, 33));//Так всякое дерево доброе приносит
console.log(str6.slice(0, 1));//Т
console.log(str6.slice(0));//Так всякое дерево доброе приносит и плоды добрые, а худое дерево приносит и плоды худые
console.log(str6.slice(4));//всякое дерево доброе приносит и плоды добрые, а худое дерево приносит и плоды худые

console.log(str6.slice(-11));//плоды худые


//SUBSTRING - возвращает строку между start / end
let str7 = 'Так всякое дерево доброе приносит и плоды добрые, а худое дерево приносит и плоды худые';
console.log(str7.substring(4, 25));//всякое дерево доброе
console.log(str7.substring(25, 4));//всякое дерево доброе
//SUBSTR - возвращает часть строки от start длины length
console.log(str7.substr(4, 20));//всякое дерево доброе
console.log(str7.substr(-11, 5)); //плоды

//СРАВНЕНИЕ СТРОК
console.log('a' > 'Z'); //true

//Возвращает код символа, нахгодящегося на указанной позиции
console.log('Ы'.codePointAt(0)); //1067

//Создает символ по его коду code
console.log(String.fromCodePoint(90)); //Z

//Создаем строку со всеми символами
let str8 = '';
for (let i = 0; i < 250; i++) {
    str8 += String.fromCodePoint(i);
}
console.log(str8);

//ЗАДАЧА 1
/*
Сделать первый символ заглавным
важность: 5
Напишите функцию ucFirst(str), возвращающую строку str с заглавным первым символом. Например:
ucFirst("вася") == "Вася";
*/
let str9 = 'bible'
function ucFirst(str9) {
    return `${str9[0].toUpperCase()}${str9.slice(1)}`;
}

console.log(ucFirst('bible'));

//ЗАДАЧА 2
/*
Проверка на спам
важность: 5
Напишите функцию checkSpam(str), возвращающую true, если str содержит 'viagra' или 'XXX', а иначе false.
Функция должна быть нечувствительна к регистру:
checkSpam('buy ViAgRA now') == true
checkSpam('free xxxxx') == true
checkSpam("innocent rabbit") == false
*/

function checkSpam(str0) {
    let strTest1 = 'xxx';
    let strTest2 = 'viagra';
    let lowerStr0 = str0.toLowerCase();
    return lowerStr0.includes(strTest1) || lowerStr0.includes(strTest2);
}

console.log(checkSpam('buy ViAgRA now'));
console.log(checkSpam('free xxxxx'));
console.log(checkSpam('innocent rabbit'));

//ЗАДАЧА 3
/*
Усечение строки
важность: 5
Создайте функцию truncate(str, maxlength), которая проверяет длину строки str и, если она превосходит maxlength, заменяет конец str на "…", так, чтобы её длина стала равна maxlength.
Результатом функции должна быть та же строка, если усечение не требуется, либо, если необходимо, усечённая строка.
Например:
truncate("Вот, что мне хотелось бы сказать на эту тему:", 20) = "Вот, что мне хотело…"
truncate("Всем привет!", 20) = "Всем привет!"
*/
function truncate(str, maxlength) {
    str.length <= maxlength
        ? console.log(str)
        : console.log(`${str.substr(0, maxlength - 1)}...`);
}

truncate("Вот, что мне хотелось бы сказать на эту тему:", 20);
truncate("Всем привет!", 20);

//ЗАДАЧА 4
/*
Выделить число
важность: 4
Есть стоимость в виде строки "$120". То есть сначала идёт знак валюты, а затем – число.
Создайте функцию extractCurrencyValue(str), которая будет из такой строки выделять числовое значение и возвращать его.
*/
function extractCurrencyValue(str) {
    return +str.slice(1);
}

console.log(extractCurrencyValue('$122'));

