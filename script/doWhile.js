// ЗАДАЧА 1
//ПОСЛЕДНЕЕ ЗНАЧЕНИЕ ЦИКЛА
/*
Какое последнее значение выведет этот код? Почему?
*/

//РЕШЕНИЕ ЗАДАЧИ 1
console.log('ЗАДАЧА 1');
let i = 3;
while (i) {
    console.log(i--); // последнее значение будет 1
}

//ЗАДАЧА 2
//Какие значения выведет цикл while?
/*Для каждого цикла запишите, какие значения он выведет. Потом сравните с ответом. */

//РЕШЕНИЕ ЗАДАЧИ 2
console.log('ЗАДАЧА 2.1');
i = 0;
while (++i < 5) {
    console.log(i); //1,2,3,4
}

console.log('ЗАДАЧА 2.2');
i = 0;
while (i++ < 5) {
    console.log(i); //0,1,2,3,4,5
}

//ЗАДАЧА 3
//Какие значения выведет цикл for?
//Для каждого цикла запишите, какие значения он выведет. Потом сравните с ответом.

//РЕШЕНИЕ ЗАДАЧИ 3
console.log('ЗАДАЧА 3.1');
for (i = 0; i < 5; i++) {
    console.log(i); // 0,1,2,3,4
}

console.log('ЗАДАЧА 3.2');
for (i = 0; i < 5; ++i) {
    console.log(i); // 0,1,2,3,4
}
/* Увеличение i++ выполняется отдельно от проверки условия (i < 5), значение i при этом не используется, поэтому нет никакой разницы между i++ и ++i.*/


//ЗАДАЧА 4
//Выведите четные числа
//При помощи цикла for выведите чётные числа от 2 до 10.

console.log('ЗАДАЧА 4');
for (i = 2; i <= 10; i++) {
    if (i % 2 === 0) {
        console.log(i);
    }
}

//ЗАДАЧА 5
//Повторять цикл, пока ввод неверен
/*
Напишите цикл, который предлагает prompt ввести число, большее 100. Если посетитель ввёл другое число – попросить ввести ещё раз, и так далее.
Цикл должен спрашивать число пока либо посетитель не введёт число, большее 100, либо не нажмёт кнопку Отмена (ESC).
Предполагается, что посетитель вводит только числа. Предусматривать обработку нечисловых строк в этой задаче необязательно.
*/

//РЕШЕНИЕ ЗАДАЧИ 5

console.log('ЗАДАЧА 5');
let num = 0;
do {
    if (num === null) {
        console.log('Отмена.');	
        break;
    }
    else num = prompt('Введите число больше 100.', '101');
} while (num <= 100);
if (num > 100) {
    console.log('Поздравляем! Вы ввели верное число!');
}

//ЗАДАЧА 6
//Вывести простые числа
/* 
Натуральное число, большее 1, называется простым, если оно ни на что не делится, кроме себя и 1.
Другими словами, n > 1 – простое, если при его делении на любое число кроме 1 и n есть остаток.
Например, 5 это простое число, оно не может быть разделено без остатка на 2, 3 и 4.
Напишите код, который выводит все простые числа из интервала от 2 до n.
Для n = 10 результат должен быть 2,3,5,7.
P.S. Код также должен легко модифицироваться для любых других интервалов.
*/

//РЕШЕНИЕ ЗАДАЧИ 6

console.log('ЗАДАЧА 6');
let n = prompt('Введите значение n больше 2:', '10');
if (n === null) {
    console.log('Отмена')
} else if (n <= 2) {
    console.log('К сожалению, вы ввели слишком маленькое число.');
} else 
    start: for (i = 2; i < n; i++) { 
        for (let j = 2; j < i ; j++) {
            if (i % j === 0) { 
                    // здесь условием (i % j === 0) сравниваются остатки
                    // от деления. Например, остаток при 6 % 2 будет 0.
                    // а остаток при делении 5 % 2 будет 1.
                continue start;
                    // метка start выводит нас из цикла на начальную позицию
                    // в обход console.log. 
                    // Таким образом console.log выводит только числа, которые
                    // делятся сами на себя, т.е. простые числа.
            }
        } 
        console.log(i);
    }
