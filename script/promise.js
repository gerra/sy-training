// ЗАДАЧА 1

// Можно ли 'перевыполнить' промис?
// Что выведет код ниже?

// РЕШЕНИЕ ЗАДАЧИ 1
console.log('ЗАДАЧА 1');

let promise = new Promise(function(resolve, reject) {
  resolve(1);
  setTimeout(() => resolve(2), 1000);
});

promise.then(console.log); // 1

// ЗАДАЧА 2
// Задержка на промисах
// Встроенная функция setTimeout использует колбэк-функции. Создайте альтернативу, использующую промисы.
// Функция delay(ms) должна возвращать промис, который перейдёт в состояние «выполнен» через ms миллисекунд, так чтобы мы могли добавить к нему .then:

// function delay(ms) {
//   // ваш код
// }

// delay(3000).then(() => alert('выполнилось через 3 секунды'));


// РЕШЕНИЕ ЗАДАЧИ 2
console.log('ЗАДАЧА 2');

function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms)) // так как мы заинтересованы только в успешном результате выполнения задачи, то передаем в промис только одну функцию, 
}

delay(3000).then(() => console.log('выполнилось через 3 секунды')); // запускается функция, к ней 

// ЗАДАЧА 3

// Анимация круга с помощью промиса
// Перепишите функцию showCircle, написанную в задании Анимация круга с помощью колбэка таким образом, чтобы она возвращала промис, вместо того чтобы принимать в аргументы функцию-callback.
// Новое использование:

// showCircle(150, 150, 100).then(div => {
//   div.classList.add('message-ball');
//   div.append('Hello, world!');
// });
// Возьмите решение из Анимация круга с помощью колбэка в качестве основы.


// РЕШЕНИЕ ЗАДАЧИ 3
console.log('ЗАДАЧА 3');

function showCircle(cx, cy, radius) {
    let div = document.createElement('div');
    div.style.width = 0;
    div.style.height = 0;
    div.style.left = `${cx}px`;
    div.style.top = `${cy}px`;
    div.className = 'circle';
    document.body.append(div);

    return new Promise (resolve => 
        setTimeout(() => {
            div.style.width = `${radius * 2}px`;
            div.style.height = `${radius * 2}px`;

            div.addEventListener(
                'transitionend', 
                handler = () => {
                    div.removeEventListener(
                        'transitionend', 
                        handler
                    ); 
                    resolve(div);
                }
            );
        })
    );
}

let go = () => {
    showCircle(150, 150, 100)
        .then(div => {
            div.classList.add('message-ball');
            div.append('Hello, world!');
        });
}
